package cn.echohawk.face1721;

import java.util.LinkedList;
import java.util.Stack;

/**
 * @description: 给定一个直方图(也称柱状图)，假设有人从上面源源不断地倒水，最后直方图能存多少水量?直方图的宽度为 1。
 * @author: echohawk
 * @create: 2021-04-25 15:18
 */
public class Solution {
	public static int trap(int[] height) {
		if (height.length < 3) {
			return 0;
		}
		int result = 0;
		int condition = 0;
		int conditionIndex = -1;
		for (int i = 0; i < height.length; i ++) {
			if (height[i] >= condition) {
				if (i - conditionIndex > 1) {
					for (int m = conditionIndex + 1; m < i; m ++) {
						result += (condition - height[m]);
					}
				}
				condition = height[i];
				conditionIndex = i;
			}
		}
		condition = 0;
		int conditionIndex2 = height.length;
		for (int i = height.length - 1; i >= conditionIndex; i --) {
			if (height[i] >= condition) {
				if (conditionIndex2 - i > 1) {
					for (int m = i + 1; m < conditionIndex2; m ++) {
						result += (condition - height[m]);
					}
				}
				condition = height[i];
				conditionIndex2 = i;
			}
		}
		return result;
	}

	public static int trapDp(int[] height) {
		if (height.length < 3) {
			return 0;
		}
		int result = 0;
		int[] leftDp = new int[height.length];
		leftDp[0] = height[0];
		int[] rigthDp = new int[height.length];
		rigthDp[height.length - 1] = height[height.length - 1];
		for (int i = 1; i < height.length; i ++) {
			leftDp[i] = Math.max(leftDp[i - 1], height[i]);
		}
		for (int j = height.length - 2; j >= 0; j --) {
			rigthDp[j] = Math.max(rigthDp[j + 1], height[j]);
		}
		for (int m = 0; m < height.length; m ++) {
			result += (Math.min(leftDp[m], rigthDp[m]) - height[m]);
		}
		return result;
	}

	public static int trapStack(int[] height) {
		int result = 0;
		Stack<Integer> stack = new Stack<>();
		for (int i = 0; i < height.length; i ++) {
			while(!stack.isEmpty() && height[stack.peek()] < height[i]) {
				Integer pop = stack.pop();
				if (stack.isEmpty()) {
					break;
				}
				result += ((i - 1 - stack.peek()) * (Math.min(height[stack.peek()], height[i]) - height[pop]));
			}
			stack.push(i);
		}
		return result;
	}

	public static int trapDoublePoint(int[] height) {
		int result = 0;
		int left = 0, right = height.length - 1;
		int leftMax = 0, rightMax = 0;
		while (left <= right) {
			rightMax = Math.max(rightMax, height[right]);
			leftMax = Math.max(leftMax, height[left]);
			if (height[left] < height[right]) {
				result += (leftMax - height[left++]);
			} else {
				result += (rightMax - height[right--]);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(trapDoublePoint(new int[]{0,1,0,2,1,0,1,3,2,1,2,1}));
	}
}
