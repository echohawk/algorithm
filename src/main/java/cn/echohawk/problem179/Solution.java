package cn.echohawk.problem179;

import com.sun.deploy.util.StringUtils;

import java.sql.SQLOutput;
import java.util.*;

/**
 * 给定一组非负整数 nums，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数。
 *
 * 注意：输出结果可能非常大，所以你需要返回一个字符串而不是整数
 */

public class Solution {

    static char[][] numCharMap = null;
    static char[] pri = new char[]{ '9','8','7','6','5','4','3','2','1','0'};
    static int[] indexSort = null;
    public static String largestNumber(int[] nums) {
        numCharMap = new char[nums.length][9];
        indexSort = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            numCharMap[i] = String.valueOf(nums[i]).toCharArray();
        }
        compareAndSort(0, 0, nums.length - 1);
        StringBuilder result = new StringBuilder();
        for (int i : indexSort) {
            result.append(String.valueOf(numCharMap[i]));
        }
        return result.toString();
    }

    private static void compareAndSort(int y, int leftX, int rightX) {
        if (leftX >= rightX ) {
            return;
        }
        Map<Character, List<Integer>> listMap = new HashMap<Character, List<Integer>>();
        for (int i = leftX; i <= rightX; i ++) {
            if (listMap.get(numCharMap[y == 0 ? i : indexSort[i]].length <= y ? (char)0 : numCharMap[y == 0 ? i : indexSort[i]][y]) == null) {
                listMap.put(numCharMap[y == 0 ? i : indexSort[i]].length <= y ? (char)0 : numCharMap[y == 0 ? i : indexSort[i]][y], new ArrayList<Integer>());
            }
            List<Integer> integers = listMap.get(numCharMap[y == 0 ? i : indexSort[i]].length <= y ? (char)0 : numCharMap[y == 0 ? i : indexSort[i]][y]);
            integers.add(y == 0 ? i : indexSort[i]);
        }
        //通过Map去找
        for (Character c : pri) {
            if (listMap.get(c) != null) {
                boolean zeroFlag = true;
                List<Integer> integers = listMap.get(c);
                List<Integer> integers0 = listMap.get((char)0);
                if ((y != 0 && c <= numCharMap[integers.get(0)][y - 1])) {
                    if (integers0 != null) {
                        zeroFlag = false;
                        for (int m = leftX; m <= (leftX + integers0.size() - 1); m ++) {
                            indexSort[m] = integers0.get(m - leftX);
                        }
                        leftX += integers0.size();
                    }
                }
                for (int m = leftX; m <= (leftX + integers.size() - 1); m ++) {
                    indexSort[m] = integers.get(m - leftX);
                }
                if (integers0 != null && zeroFlag && rightX == leftX + integers.size() - 1 + integers0.size()) {
                    for (int m = leftX; m <= (leftX + integers0.size() - 1); m ++) {
                        indexSort[m] = integers0.get(m - leftX);
                    }
                    leftX += integers0.size();
                }

                compareAndSort(y + 1, leftX, leftX + integers.size() - 1);
                leftX += integers.size();
            }
        }
    }

    public static String thoughtlargestNumber(int[] nums) {
        String[] strings = new String[nums.length];
        for (int i = 0; i < nums.length; i ++) {
            strings[i] = String.valueOf(nums[i]);
        }
//        Arrays.sort(strings, (o1, o2) -> (o2 + o1).compareTo(o1 + o2));
        StringBuilder sb = new StringBuilder();
        for (String string : strings) {
            sb.append(string);
        }
        if (sb.toString().charAt(0) == '0') {
            return "0";
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(thoughtlargestNumber(new int[]{432,43243}));
    }
}
