package cn.echohawk.problem54;

import java.util.ArrayList;
import java.util.List;

/**
 * 给你一个 m 行 n 列的矩阵 matrix ，请按照 顺时针螺旋顺序 ，返回矩阵中的所有元素。
 */
public class Solution {

    public List<Integer> spiralOrder(int[][] matrix) {
//        int[][] matrix;
        int up = 0, right = matrix[0].length - 1, down = matrix.length - 1, left = 0;
//        this.matrix = matrix;
        int size = matrix.length * matrix[0].length;
        List<Integer> result = new ArrayList<>(size);
        int direction = 1;
        while (result.size() != size) {
            if (direction == 1) {
                for (int i = left; i <= right; i++) {
                    result.add(matrix[up][i]);
                }
                up++;
            } else if (direction == 2) {
                for (int i = up; i <= down; i++) {
                    result.add(matrix[i][right]);
                }
                right--;
            } else if (direction == 3) {
                for (int i = right; i >= left; i--) {
                    result.add(matrix[down][i]);
                }
                down--;
            } else if (direction == 4) {
                for (int i = down; i >= up; i--) {
                    result.add(matrix[i][left]);
                }
                left++;
            }
            if (++direction == 5) {
                direction = 1;
            }
        }
        return result;
    }

}
