package cn.echohawk.problem456;

import java.util.Stack;

/**
 * @description: 给你一个整数数组 nums ，数组中共有 n 个整数。132 模式的子序列 由三个整数 nums[i]、nums[j] 和 nums[k] 组成，并同时满足：i < j < k 和 nums[i] < nums[k] < nums[j] 。  如果 nums 中存在 132 模式的子序列 ，返回 true ；否则，返回 false 。  来源：力扣（LeetCode） 链接：https://leetcode-cn.com/problems/132-pattern 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-11 19:03
 */
public class Solution {
	public boolean find132pattern(int[] nums) {
		Stack<Integer> stack = new Stack<>();
		int res = Integer.MIN_VALUE;
		for (int i = nums.length - 1; i >= 0; i--) {
			if (nums[i] < res) {
				return true;
			}
			while (!stack.isEmpty() && nums[i]  > stack.peek()) {
				res = Math.max(res, stack.pop());
			}
			stack.push(nums[i]);
		}
		return false;
	}
}
