package cn.echohawk.problem90;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.*;

/**
 * @description: 给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。
 * 解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/subsets-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-28 17:09
 */
public class Solution {

	private static List<List<Integer>> result = new ArrayList<>();
	private static List<Integer> list = new ArrayList<>();
	private static Map<Integer, Integer> map = new HashMap<>();
	public static List<List<Integer>> subsetsWithDup(int[] nums) {
		for (int num : nums) {
			Integer integer = map.get(num);
			if (integer == null) {
				integer = 0;
				list.add(num);
			}
			map.put(num, ++integer);
		}
		List<Integer> base = new ArrayList<>(0);
		deep(base, 0);
		return result;
	}

	private static void deep(List<Integer> base, int index) {
		if (index == list.size()) {
			result.add(base);
			return;
		}
		Integer integer = map.get(list.get(index));
		for (int i = integer; i >= 0; i --) {
			int temp = i;
			if (i == 0) {
				deep(base, index + 1);
			} else {
				List<Integer> newList = new ArrayList<>(base.size() + i);
				for (Integer integer1 : base) {
					newList.add(integer1);
				}
				while (temp > 0) {
					newList.add(list.get(index));
					temp--;
				}
				deep(newList, index + 1);
			}
		}
	}

	public static void main(String[] args) {
		System.out.println(subsetsWithDup(new int[]{1,2,2}));
	}


}
