package cn.echohawk.problem1882;

import com.alibaba.fastjson.JSON;

import java.util.Comparator;
import java.util.PriorityQueue;

public class Solution {
    public static int[] assignTasks(int[] servers, int[] tasks) {
        PriorityQueue<int[]> priorityQueue = new PriorityQueue<int[]>((o1, o2) -> {
            int i = o1[1] - o2[1];
            if (i == 0) {
                return o1[0] - o2[0];
            }
            return i;
        });
        PriorityQueue<int[]> workQueue = new PriorityQueue<int[]>((o1, o2) -> o1[2] - o2[2]);
        for (int i = 0; i < servers.length; i++) {
            priorityQueue.add(new int[]{i, servers[i], 0});
        }
        System.out.println(JSON.toJSONString(workQueue));
        int[] result = new int[tasks.length];
        int taskOffset = 0;
        for (int i = 0; i < tasks.length + taskOffset; i++) {
            while (!workQueue.isEmpty() && workQueue.peek()[2] == i) {
                priorityQueue.add(workQueue.poll());
            }
            if (priorityQueue.isEmpty()) {
                taskOffset++;
                continue;
            }
            int[] poll = priorityQueue.poll();
            result[i - taskOffset] = poll[0];
            poll[2] = tasks[i - taskOffset] + i;
            workQueue.add(poll);
        }
        return result;
    }

    public static void main(String[] args) {
//        [74,57,61,82,67,97,67,21,61,79,21,50,14,88,48,52,76,64]
//[21,100,48,64,20,8,28,10,3,63,7]
        System.out.println(JSON.toJSONString(assignTasks(new int[]{74,57,61,82,67,97,67,21,61,79,21,50,14,88,48,52,76,64}, new int[]{21,100,48,64,20,8,28,10,3,63,7})));
    }
}
