package cn.echohawk.problem61;

/**
 * @description: 给你一个链表的头节点 head ，旋转链表，将链表每个节点向右移动 k 个位置。
 * @author: echohawk
 * @create: 2021-05-03 16:44
 */
public class Solution {

	public ListNode rotateRight(ListNode head, int k) {
		ListNode font = head, back = head;
		int flag = 0;
		while (true) {
			flag = Math.min(k, ++flag);
			if (flag == k) {
				back = back.next;
			}
			font = font.next;
			if (font.next == null) {
				font.next = head;
			}
			if (font == head) {
				break;
			}
		}
		System.out.println();
		if (flag == k) {
			head = back.next;
			back.next = null;
			return head;
		} else {
			k = k % flag;
			flag = 0;
			while (true) {
				if (flag == k) {
					back = back.next;
				}
				flag = Math.min(++flag, k);
				font = font.next;
				if (font == head) {
					head = back.next;
					back.next = null;
					return head;
				}
			}
		}
	}
}
