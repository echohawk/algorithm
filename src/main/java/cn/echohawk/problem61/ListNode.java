package cn.echohawk.problem61;

/**
 * @description: listNode
 * @author: echohawk
 * @create: 2021-05-03 16:46
 */
public class ListNode {
	public int val;
	public ListNode next;
	public ListNode() {}
	public ListNode(int val) { this.val = val; }
	public ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
