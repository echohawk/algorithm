package cn.echohawk.problem1723;

import java.util.Arrays;

/**
 * @description: 给你一个整数数组 jobs ，其中 jobs[i] 是完成第 i 项工作要花费的时间。
 * 请你将这些工作分配给 k 位工人。所有工作都应该分配给工人，且每项工作只能分配给一位工人。
 * 工人的 工作时间 是完成分配给他们的所有工作花费时间的总和。请你设计一套最佳的工作分配方案，
 * 使工人的 最大工作时间 得以 最小化 。  返回分配方案中尽可能 最小 的 最大工作时间 。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/find-minimum-time-to-finish-all-jobs
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-08 11:06
 */
public class Solution {
	/**
	 * 错误的类贪心算法
	 * @param jobs
	 * @param k
	 * @return
	 */
	public static int minimumTimeRequired(int[] jobs, int k) {
		Arrays.sort(jobs);
		if (k >= jobs.length) {
			return jobs[jobs.length - 1];
		}
		int[] worker = new int[k];
		boolean direction = true;
		int i = 0;
		for (int i1 = jobs.length - 1; i1 >= 0; i1--) {
			worker[0] += jobs[i1];
			Arrays.sort(worker);
		}
		return worker[k - 1];
	}

	/**
	 * 一次错误的尝试
	 */
	private static int result = Integer.MAX_VALUE;
	private static int k = 0;
	private static int[] jobs = null;

	public static int minimumTimeRequiredDfs(int[] jobs_s, int k_s) {
		int[] sum = new int[k];
		k = k_s;
		jobs = jobs_s;
		dfs(0, sum,  0, k_s);
		return result;
	}

	private static void dfs(int i, int[] sum , int max, int noTask) {
		if (max >= result) return;
		if (i == jobs.length) {
			result = max;
			return;
		}
		if (noTask > k - i) {
			return;
		}
		for (int index = 0; index < k; index++) {
			sum[index] += jobs[i];
			dfs(i + 1, sum, Math.max(max, sum[index]), sum[index] == jobs[i] ? ++noTask : noTask);
			sum[index] -= jobs[i];
		}
	}

	public static int minimumTimeRequiredDfsCut(int[] jobs_s, int k_s) {
		int[] sum = new int[k_s];
		k = k_s;
		jobs = jobs_s;
		dfsCut(0, sum,  0, 0);
		return result;
	}

	private static void dfsCut(int i, int[] sum , int max, int user) {
		if (max >= result) return;
		if (i == jobs.length) {
			result = max;
			return;
		}
		if (user < k) {
			sum[user] = jobs[i];
			dfs(i + 1, sum, Math.max(max, sum[user]), user + 1);
			sum[user] = 0;
		}
		//这里为user的原因是只为上面已经分配的进行分配 如果为k的话就会导致重算问题
		for (int index = 0; index < user; index++) {
			sum[index] += jobs[i];
			dfs(i + 1, sum, Math.max(max, sum[index]), user);
			sum[index] -= jobs[i];
		}
	}


	public static void main(String[] args) {
		System.out.println(minimumTimeRequired(new int[]{9899456,8291115,9477657,9288480,5146275,7697968,8573153,3582365,3758448,9881935,2420271,4542202
		}, 9));
	}
}
