package cn.echohawk.problem5787;

public class Solution {
    private int[][] grid1 = null;
    private int[][] grid2 = null;
    public int countSubIslands(int[][] _grid1, int[][] _grid2) {
        int result = 0;
        grid1 = _grid1;
        grid2 = _grid2;
        for (int i = 0; i < grid2.length; i++) {
            for (int j = 0; j < grid2[i].length; j++) {
                if (grid2[i][j] == 1) {
                    if (dfs(i, j)) {
                        result ++;
                    }
                }
            }
        }
        return result;
    }

    private boolean dfs(int x, int y) {
        boolean result = true;
        if (grid1[x][y] != 1) {
            result = false;
        }
        grid2[x][y] = 0;
        if (x > 0 && grid2[x - 1][y] == 1) {
            result = dfs(x - 1, y) && result;
        }
        if (y > 0 && grid2[x][y - 1] == 1) {
            result = dfs(x, y - 1) && result;
        }
        if (x < grid2.length - 1 && grid2[x + 1][y] == 1) {
            result = dfs(x + 1, y) && result;
        }
        if (y < grid2[0].length - 1 && grid2[x][y + 1] == 1) {
            result = dfs(x, y + 1) && result;
        }
        return result;
    }
}
