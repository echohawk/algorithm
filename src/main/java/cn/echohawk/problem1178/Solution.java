package cn.echohawk.problem1178;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 猜灯谜
 * @author: echohawk
 * @create: 2021-06-08 17:10
 */
public class Solution {
	public static List<Integer> findNumOfValidWords(String[] words, String[] puzzles) {
		List<Integer> result = new ArrayList<>();
		Map<Integer, Integer> map = new HashMap<>();
		for (String word : words) {
			int temp = 0;
			for (char aChar : word.toCharArray()) {
				temp = temp | (0x01 << (aChar - 'a'));
			}
			map.put(temp, map.getOrDefault(temp, 0) + 1);
		}
		for (String puzzle : puzzles) {
			int count = 0;
			char[] chars = puzzle.toCharArray();
			for (int i = 0; i < (1 << 6); i++) {
				int temp = 0x01 << (chars[0] - 'a');
				for (int j = 0; j < chars.length - 1; j++) {
					if ((i & (1 << j)) != 0) {
						temp = temp | (1 << (chars[j + 1] - 'a'));
					}
				}
				count += map.getOrDefault(temp, 0);
			}
			result.add(count);
		}
		return result;
	}
}
