package cn.echohawk.problem115;

import com.alibaba.fastjson.JSON;

/**
 * @description: 给定一个字符串 s 和一个字符串 t ，计算在 s 的子序列中 t 出现的个数。
 * 字符串的一个 子序列 是指，通过删除一些（也可以不删除）字符且不干扰剩余字符相对位置所组成的新字符串。
 * （例如，"ACE" 是 "ABCDE" 的一个子序列，而 "AEC" 不是）
 * 题目数据保证答案符合 32 位带符号整数范围。   
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/distinct-subsequences
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-31 10:45
 */
public class Solution {
	public static int numDistinct(String s, String t) {
		if (s.length() <= t.length()) {
			return s.equals(t) ? 1 : 0;
		}
		int[][] dp = new int[s.length() + 1][t.length() + 1];
		char[] sChar = s.toCharArray();
		char[] tChar = t.toCharArray();
		for (int x = 0; x <= sChar.length; x++){
			dp[x][0] = 1;
		}
		for (int x = 1; x <= sChar.length; x++) {
			for (int y = 1; y <= tChar.length; y++) {
				dp[x][y] = dp[x - 1][y];
				if (tChar[y - 1] == sChar[x - 1]) {
					dp[x][y] += dp[x-1][y-1];
				}
			}
		}
		System.out.println(JSON.toJSONString(dp));
		return dp[s.length()][t.length()];
	}

	public static void main(String[] args) {
		System.out.println(numDistinct("rabbbit",
				"rabbit"));
	}
}
