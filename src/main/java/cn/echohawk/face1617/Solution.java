package cn.echohawk.face1617;

/**
 * 给定一个整数数组，找出总和最大的连续数列，并返回总和。
 */
public class Solution {
    //使用分治算法
    public static int maxSubArray(int[] nums) {
        return mergeArrray(0, nums.length - 1, nums);
    }

    private static int mergeArrray(int start, int end, int[] nums) {
        if (start == end) {
            return nums[start];
        }
        int mid = start + ((end - start) >> 1);
        int leftRange = mergeArrray(start, mid, nums);
        int rightRange = mergeArrray(mid + 1, end, nums);
        int maxLeft = Integer.MIN_VALUE;
        int maxRight = Integer.MIN_VALUE;
        int sumLeft = 0;
        int sumRigth = 0;
        //中线分割
        for (int left = mid; left >= start; left --) {
            maxLeft = Math.max(maxLeft, sumLeft += nums[left]);
        }
        for (int right = mid + 1; right <= end; right ++) {
            maxRight = Math.max(maxRight, sumRigth += nums[right]);
        }
        return Math.max(Math.max(leftRange,rightRange), maxRight + maxLeft);
    }

    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
    }
}
