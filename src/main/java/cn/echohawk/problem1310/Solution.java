package cn.echohawk.problem1310;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 有一个正整数数组 arr，现给你一个对应的查询数组 queries，其中 queries[i] = [Li, Ri]。
 * 对于每个查询 i，请你计算从 Li 到 Ri 的 XOR 值（即 arr[Li] xor arr[Li+1] xor ... xor arr[Ri]）作为本次查询的结果。
 * 并返回一个包含给定查询 queries 所有结果的数组。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/xor-queries-of-a-subarray
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-12 11:56
 */
public class Solution {
	static Map<Integer, List<Integer>> indexMap = new HashMap<>();
	public static int[] xorQueries(int[] arr, int[][] queries) {
		for (int i = 0; i < queries.length; i++) {
			addMap(queries[i][0], i, true);
			addMap(queries[i][1], i, false);
		}
		int[] result = new int[queries.length];
		int numXOR = 0;
		for (int i = 0; i <= arr.length; i++) {
			List<Integer> integers = indexMap.get(i - 1);
			if (integers != null) {
				for (Integer integer : integers) {
					result[integer] ^= numXOR;
				}
			}
			if (arr.length != i)
				numXOR^=arr[i];
		}
		return result;
	}

	private static void addMap(int query, int index, boolean isStart) {
		List<Integer> integers = null;
		integers = indexMap.get(isStart ? query - 1 : query);
		if (integers == null) {
			integers = new ArrayList<>();
		}
		integers.add(index);
		indexMap.put(isStart ? query - 1 : query, integers);
	}

	public static void main(String[] args) {
		xorQueries(new int[]{16}, new int[][]{{0,0},{0,0},{0,0}});
	}
}
