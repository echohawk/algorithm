package cn.echohawk.hanoi;

public class Code01_Hanoi {

    //经典递归方法 总共把问题拆成三步
    // 第一步  把n-1从起始位置移到一个其他位置
    // 第二步  把n从起始位置移到目标位置
    // 第三步  把n-1从其他位置移到目标位置
    public static void main(String[] args) {
        leftToRight(3);
        System.out.println("===============");
        hanoi(3, "左", "右", "中");
    }

    public static void hanoi(int n, String from, String to, String other) {
        if (n == 1) {
            System.out.println("把第1块从"+from+"->"+ to);
            return;
        }
        //首先要把n-1移到中间
        hanoi(n - 1, from, other, to);
        System.out.println("把第"+ n +"块"+from+"->"+ to);
        hanoi(n - 1, other, to, from);
    }

    public static void leftToRight(int n) {
        if (n == 1) {
            System.out.println("把第1块从左->右");
            return;
        }
        //首先要把n-1移到中间
        leftToMid(n - 1);
        System.out.println("把第"+ n +"块从左->右");
        midToRight(n - 1);
    }

    private static void midToRight(int n) {
        if (n == 1) {
            System.out.println("把第1块从中->右");
            return;
        }
        midToLeft(n - 1);
        System.out.println("把第"+ n +"块从中->右");
        leftToRight(n - 1);
    }

    private static void midToLeft(int n) {
        if (n == 1) {
            System.out.println("把第1块从中->左");
            return;
        }
        midToRight(n - 1);
        System.out.println("把第"+ n +"块从中->左");
        rightToLeft(n - 1);
    }

    private static void rightToLeft(int n) {
        if (n == 1) {
            System.out.println("把第1块从右->左");
            return;
        }
        rightToMid(n - 1);
        System.out.println("把第"+ n +"块从右->左");
        midToLeft(n - 1);
    }

    private static void leftToMid(int n) {
        if (n == 1) {
            System.out.println("把第1块从左->中");
            return;
        }
        //首先要把n-1移到中间
        leftToRight(n - 1);
        System.out.println("把第"+ n +"块从左->中");
        rightToMid(n - 1);
    }

    private static void rightToMid(int n) {
        if (n == 1) {
            System.out.println("把第1块从右->中");
            return;
        }
        //首先要把n-1移到中间
        rightToLeft(n - 1);
        System.out.println("把第"+ n +"块从右->中");
        leftToMid(n - 1);
    }
}
