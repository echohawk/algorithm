package cn.echohawk.problem263;

/**
 * 给你一个整数 n ，请你判断 n 是否为 丑数 。如果是，返回 true ；否则，返回 false 。
 *
 * 丑数 就是只包含质因数 2、3 和/或 5 的正整数。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/ugly-number
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static boolean isUgly(int n) {
        if (n > 0) {
            while ((n & 0x01) != 1) {
                n = (n >> 1);
            }
            while (n != 1) {
                if (n % 3 == 0) {
                    n /= 3;
                } else if (n % 5 == 0) {
                    n /= 5;
                } else if (n != 1) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isUgly(14));
    }
}
