package cn.echohawk.problem781;

import java.util.HashMap;

/**
 * 森林中，每个兔子都有颜色。其中一些兔子（可能是全部）告诉你还有多少其他的兔子和自己有相同的颜色。我们将这些回答放在 answers 数组里。
 *
 * 返回森林中兔子的最少数量。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/rabbits-in-forest
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static int numRabbits(int[] answers) {
        int result = 0;
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int answer : answers) {
            Integer integer = map.get(answer);
            if (integer == null) {
                integer = 0;
            }
            map.put(answer, ++integer);
        }
        for (Integer integer : map.keySet()) {
            int multi = ((map.get(integer) - 1) / (integer + 1)) + 1;
            result += (integer + 1) * multi;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(numRabbits(new int[]{1, 1, 2}));
    }
}
