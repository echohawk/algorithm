package cn.echohawk.problem80;

/**
 * @description: 给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 最多出现两次 ，返回删除后数组的新长度。
 * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-06 11:03
 */
public class Solution {
	public static int removeDuplicates(int[] nums) {
		if (nums.length < 3) {
			return nums.length;
		}
		boolean repeatFlag = false;
		int repeatValue = nums[0];
		int offset = 0;
		for(int i = 1; i < nums.length; i ++) {
			if (nums[i] == repeatValue) {
				if (repeatFlag) {
					offset++;
				} else {
					repeatFlag = true;
				}
			} else {
				repeatFlag = false;
				repeatValue = nums[i];
			}
			if (offset != 0) {
				nums[i - offset] = nums[i];
			}
		}
		return nums.length - offset;
	}

	public static int bestRemoveDuplicates(int[] nums) {
		int r = 0;
		for (int n : nums) {
			if (r < 2 || n > nums[r - 2]) {
				nums[r++] = n;
			}
		}
		return r;
	}

	public static void main(String[] args) {
		System.out.println(bestRemoveDuplicates(new int[]{0,0,1,1,1,1,2,3,3}));
	}
}
