package cn.echohawk.problem73;

import com.alibaba.fastjson.JSON;

/**
 * 给定一个 m x n 的矩阵，如果一个元素为 0 ，则将其所在行和列的所有元素都设为 0 。请使用 原地 算法。
 *
 * 进阶：
 *
 * 一个直观的解决方案是使用  O(mn) 的额外空间，但这并不是一个好的解决方案。
 * 一个简单的改进方案是使用 O(m + n) 的额外空间，但这仍然不是最好的解决方案。
 * 你能想出一个仅使用常量空间的解决方案吗？
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/set-matrix-zeroes
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static void setZeroes(int[][] matrix) {
        //两个标记
        boolean xZeroFlag = false;
        boolean yZeroFlag = false;
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[x].length; y++) {
                if (matrix[x][y] == 0) {
                    if (x == 0) xZeroFlag = true;
                    if (y == 0) yZeroFlag = true;
                    matrix[x][0] = 0;
                    matrix[0][y] = 0;
                }
            }
        }
        for (int i = 1; i < matrix.length; i++) {
            if (matrix[i][0] == 0) {
                for (int y = 1; y < matrix[i].length; y++) {
                    matrix[i][y] = 0;
                }
            }
        }
        for (int i = 1; i < matrix[0].length; i++) {
            if (matrix[0][i] == 0) {
                for (int y = 1; y < matrix.length; y++) {
                    matrix[y][i] = 0;
                }
            }
        }
        if (xZeroFlag) {
            for (int y = 0; y < matrix[0].length; y++) {
                matrix[0][y] = 0;
            }
        }
        if (yZeroFlag) {
            for (int y = 0; y < matrix.length; y++) {
                matrix[y][0] = 0;
            }
        }
    }

    public static void main(String[] args) {
        setZeroes(new int[][]{{1,0,3}});
    }
}
