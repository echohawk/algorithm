package cn.echohawk.problem28;

import com.alibaba.fastjson.JSON;

/**
 * @description: 实现 strStr() 函数。
 * 给你两个字符串 haystack 和 needle ，请你在 haystack 字符串中找出 needle 字符串出现的第一个位置（下标从 0 开始）。
 * 如果不存在，则返回  -1 。   
 * 说明：  当 needle 是空字符串时，我们应当返回什么值呢？这是一个在面试中很好的问题。
 * 对于本题而言，当 needle 是空字符串时我们应当返回 0 。
 * 这与 C 语言的 strstr() 以及 Java 的 indexOf() 定义相符。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/implement-strstr
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-20 10:59
 */
public class Solution {
	/**
	 * 暴力算法
	 * @param haystack
	 * @param needle
	 * @return
	 */
	public static int strStr(String haystack, String needle) {
		if ("".equals(needle)) {
			return 0;
		}
		int len = needle.length();
		char[] chars = needle.toCharArray();
		char[] hayChar = haystack.toCharArray();
		a : for (int i = 0; i < haystack.length(); i ++) {
			if (chars[0] == hayChar[i]) {
				int tempIndex = i + len;
				while (tempIndex != i) {
					if (tempIndex > haystack.length()) {
						return -1;
					}
					if (chars[tempIndex - i - 1] != hayChar[--tempIndex]) {
						continue a;
					}
				}
				return i;
			}
		}
		return -1;
	}


	/**
	 * KMP算法
	 * @param haystack
	 * @param needle
	 * @return
	 */
	public static int kmpStrStr(String haystack, String needle) {
		if ("".equals(needle)) {
			return 0;
		}
		int[] kmp = new int[needle.length()];
		char[] chars = needle.toCharArray();
		char[] hayChars = haystack.toCharArray();
		//暴力求kmp之我是傻逼
//		for (int i = 1; i < needle.length() - 1; i ++) {
//			for (int y = 1; y <= i; y ++) {
//				if (chars[0] == chars[y] && chars[i - y] == chars[i]) {
//					int gap = 1;
//					while (y + gap <= i && chars[gap] == chars[y + gap]) {
//						gap++;
//					}
//					if (y + gap > i) {
//						kmp[i + 1] = gap;
//						break;
//					}
//				}
//			}
//		}
		//DP求解
		for (int m = 1; m < chars.length; m ++) {
			if (chars[kmp[m - 1]] == chars[m]) {
				kmp[m] = kmp[m - 1] + 1;
			} else {
				int kmpInd = kmp[kmp[m - 1] <= 0 ? 0 : kmp[m - 1] -1];
				while (kmpInd >= 0 && chars[kmpInd] != chars[m] && --kmpInd >= 0) {
					kmpInd = kmp[kmpInd];
				}
				kmp[m] = kmpInd < 0 ? 0 : kmpInd + 1;
			}
		}
		System.arraycopy(kmp, 0, kmp, 1, kmp.length - 1);
		kmp[0] = -1;

		int kmpPoint = 0;
		int hayPoint = 0;
		for (; hayPoint < hayChars.length && kmpPoint < needle.length(); hayPoint ++) {
			if (chars[kmpPoint] == hayChars[hayPoint]) {
				kmpPoint++;
			} else if (kmpPoint != 0){
				kmpPoint = kmp[kmpPoint];
				hayPoint--;
			}
		}
		if (kmpPoint == needle.length()) {
			return hayPoint - needle.length();
		}
		return -1;
	}

	/**
	 * 测试代码
	 * @param chars
	 */
	public static void kmp(char[] chars) {
		int[] kmp = new int[chars.length];
		for (int m = 1; m < chars.length; m ++) {
			if (chars[kmp[m - 1]] == chars[m]) {
				kmp[m] = kmp[m - 1] + 1;
			} else {
				int kmpInd = kmp[kmp[m - 1] <= 0 ? 0 : kmp[m - 1] -1];
				while (kmpInd >= 0 && chars[kmpInd] != chars[m] && --kmpInd >= 0) {
					kmpInd = kmp[kmpInd];
				}
				kmp[m] = kmpInd < 0 ? 0 : kmpInd + 1;
			}
		}
		System.arraycopy(kmp, 0, kmp, 1, kmp.length - 1);
		kmp[0] = -1;
	}



	public static void main(String[] args) {
//		System.out.println(kmpStrStr("aabaaabaaac",
//				"aabaaac"));
		String a = "lsuwwuovcnor";
		kmp(a.toCharArray());
	}
}
