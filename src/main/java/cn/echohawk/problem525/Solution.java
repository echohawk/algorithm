package cn.echohawk.problem525;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 给定一个二进制数组 nums , 找到含有相同数量的 0 和 1 的最长连续子数组，并返回该子数组的长度。
 * @author: echohawk
 * @create: 2021-06-03 11:42
 */
public class Solution {
	public static int findMaxLength(int[] nums) {
		int result = 0;
		Map<Integer, Integer> map = new HashMap<>();
		map.put(0, 0);
		int[] sum = new int[nums.length + 1];
		for (int i = 0; i < nums.length; i++) {
			sum[i + 1] = sum[i] + 2 * nums[i];
			//添加
			int gap = sum[i + 1] - (i + 1);
			if (map.get(gap) == null) {
				map.put(gap, i + 1);
			} else {
				result = Math.max(result, i - map.get(gap) + 1);
			}
		}
		System.out.println(JSON.toJSONString(map));
		return result;
	}

	public static void main(String[] args) {
		System.out.println(findMaxLength(new int[]{0,1}));
	}
}
