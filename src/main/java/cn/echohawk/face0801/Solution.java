package cn.echohawk.face0801;

import java.util.HashMap;
import java.util.Map;

/**
 * 三步问题。有个小孩正在上楼梯，楼梯有n阶台阶，小孩一次可以上1阶、2阶或3阶。实现一种方法，计算小孩有多少种上楼梯的方式。结果可能很大，你需要对结果模1000000007。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/three-steps-problem-lcci
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    private static Map<Integer, Long> map = new HashMap<>();
    static {
        map.put(1, 1L);
        map.put(2, 2L);
        map.put(3, 4L);
    }
    public static int waysToStep(int n) {
        return (int)fibonacci(n);
    }
    private static long fibonacci(int n) {
        if (n < 1) {
            return 0;
        }
        Long integer = map.get(n);
        if (integer == null) {
            long i = (fibonacci(n - 3) + fibonacci(n - 2) + fibonacci(n - 1)) % 1000000007;
            map.put(n, i);
            return i;
        }
        return integer;
    }

    static int[] dp = new int[]{1,2,4};
    public static int waysToStepDp(int n) {
        if (n < 4) {
            return dp[n - 1];
        }
        int offset = -1;
        for (int i = 4; i <= n; i ++) {
            if (++offset > 2) {
                offset = 0;
            }
            dp[offset] = (((dp[0] + dp[1])%1000000007) + dp[2])%1000000007;
        }
        return dp[offset];
    }

    public static void main(String[] args) {
        long l = System.currentTimeMillis();
        System.out.println(waysToStepDp(76));
        long l1 = System.currentTimeMillis();
        System.out.println(l1 - l);
    }
}
