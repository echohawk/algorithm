package cn.echohawk.problem12;

/**
 * @description: 罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
 * @author: echohawk
 * @create: 2021-05-14 11:50
 */
public class Solution {
	public String intToRoman(int num) {
		int[] numLevel = new int[]{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
		String[] strLevel = new String[]{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
		StringBuilder sb = new StringBuilder();
		int index = 0;
		while (num != 0) {
			if (num >= numLevel[index]) {
				num-=numLevel[index];
				sb.append(strLevel[index]);
			} else {
				index++;
			}
		}
		return sb.toString();
	}


	public int RomanToint(String roman) {
		int[] numLevel = new int[]{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
		String[] strLevel = new String[]{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
		int result = 0;
		int index = 0;
		char[] chars = roman.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == strLevel[index].charAt(0)) {
				if (strLevel[index].length() == 2){
					if (i < chars.length - 1 && chars[i + 1] == strLevel[index].charAt(1)) {
						result += numLevel[index];
						i++;
						continue;
					}
				} else {
					result += numLevel[index];
					continue;
				}
			}
			index++;
			i--;
		}
		return result;
	}
}
