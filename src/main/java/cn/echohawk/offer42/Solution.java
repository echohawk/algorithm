package cn.echohawk.offer42;

/**
 *输入一个整型数组，数组中的一个或连续多个整数组成一个子数组。求所有子数组的和的最大值。
 *
 * 要求时间复杂度为O(n)。
 */
public class Solution {
    public int maxSubArray(int[] nums) {
        int res = nums[0];
        int max = 0;
        for (int num : nums) {
            max += num;
            if (max > 0) {
                res = Math.max(res, max);
            } else {
                res = Math.max(res, num);
                max = 0;
            }
        }
        return res;
    }

    public int bigMaxSubArray(int[] nums) {
        int res = nums[0];
        int max = 0;
        for (int num : nums) {
            if (max > 0) {
                max += num;
            } else {
                max = num;
            }
            res = Math.max(res, max);
        }
        return res;
    }
}
