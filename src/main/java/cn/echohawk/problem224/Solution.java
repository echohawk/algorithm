package cn.echohawk.problem224;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @description: 给你一个字符串表达式 s ，请你实现一个基本计算器来计算并返回它的值。
 * @author: echohawk
 * @create: 2021-06-02 19:04
 */
public class Solution {
	public int calculate(String s) {
		return cal(s);
	}
	int index = 0;
	private int cal(String s) {
		Deque<Integer> stack = new LinkedList<>();
		char[] chars = s.toCharArray();
		char oper = '+';
		int num = 0;
		while (index < chars.length) {
			char c = chars[index++];
			if (Character.isDigit(c)) {
				num = num * 10 + (c - '0');
			}
			if (c == '(') {
				num = cal(s);
			}
			if ((!Character.isDigit(c) && c != ' ') || index == chars.length) {
				switch (oper) {
					case '+' : stack.push(num);
					break;
					case '-' : stack.push(-num);
					break;
					case '*' : stack.push(stack.pop() * num);
					break;
					case '/' : stack.push(stack.pop() / num);
					break;
				}
				oper = c;
				num = 0;
			}
			if (c == ')') {
				break;
			}
		}
		int result = 0;
		while (!stack.isEmpty()) {
			result += stack.pop();
		}
		return result;

	}

}
