package cn.echohawk.problem208;


/**
 * Trie（发音类似 "try"）或者说 前缀树 是一种树形数据结构，用于高效地存储和检索字符串数据集中的键。这一数据结构有相当多的应用情景，例如自动补完和拼写检查。
 *
 * 请你实现 Trie 类：
 *
 * Trie() 初始化前缀树对象。
 * void insert(String word) 向前缀树中插入字符串 word 。
 * boolean search(String word) 如果字符串 word 在前缀树中，返回 true（即，在检索之前已经插入）；否则，返回 false 。
 * boolean startsWith(String prefix) 如果之前已经插入的字符串 word 的前缀之一为 prefix ，返回 true ；否则，返回 false 。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/implement-trie-prefix-tree
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Trie {
    private class TrieNode { // 每个节点最多有26个不同的小写字母
        private boolean isEnd;
        private TrieNode[] next;

        public TrieNode() {
            isEnd = false;
            next = new TrieNode[26];
        }

    }

    private TrieNode node;

    /** Initialize your data structure here. */
    public Trie() {
        this.node = new TrieNode();
    }

    /** Inserts a word into the trie. */
    public void insert(String word) {
        if (word == null) {
            return;
        }
        char[] chars = word.toCharArray();
        TrieNode[] temp = node.next;
        for (int i = 0; i < chars.length; i ++) {
            if (temp[chars[i] - 'a'] == null) {
                temp[chars[i] - 'a'] = new TrieNode();
            }
            if (i == chars.length - 1) {
                temp[chars[i] - 'a'].isEnd = true;
                break;
            }
            temp = temp[chars[i] - 'a'].next;
        }
    }

    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        if (word == null) {
            return false;
        }
        TrieNode[] temp = node.next;
        char[] chars = word.toCharArray();
        for (int i = 0; i < chars.length; i ++) {
            if (temp[chars[i] - 'a'] == null) {
                break;
            }
            if (i == chars.length - 1 && temp[chars[i] - 'a'].isEnd) {
                return true;
            }
            temp = temp[chars[i] - 'a'].next;
        }
        return false;
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        if (prefix == null) {
            return false;
        }
        TrieNode[] temp = node.next;
        char[] chars = prefix.toCharArray();
        for (int i = 0; i < chars.length; i ++) {
            if (temp[chars[i] - 'a'] == null) {
                break;
            }
            if (i == chars.length - 1) {
                return true;
            }
            temp = temp[chars[i] - 'a'].next;
        }
        return false;
    }
}
