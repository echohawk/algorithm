package cn.echohawk.problem122;

/**
 * 给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
 *
 * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
 *
 * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static int maxProfit(int[] prices) {
        if (prices.length < 2) {
            return 0;
        }
        prices[0] = Math.max(prices[1] - prices[0], 0);
        for (int i = 1; i < prices.length - 1; i ++) {
            prices[0] += Math.max(prices[i + 1] - prices[i], 0);
        }
        return prices[0];
    }

    /**
     * 尝试用Dp解
     * @param prices
     * @return
     */
    public static int maxProfitDp(int[] prices) {
        int[] dp = new int[prices.length];
        for (int i = 1; i < prices.length; i ++) {
            dp[i] = dp[i - 1] + Math.max(prices[i] - prices[i - 1], 0);
        }
        return dp[prices.length - 1];
    }

    /**
     * 大触的DP [天数， 持有状态0未持有 1已持有]
     * @param prices
     */
    public static int maxProfitBigCoding(int[] prices) {
        int[][] dp = new int[prices.length][2];
        dp[0][0] = 0;
        dp[0][1] = -prices[0];
        for (int i = 1; i < prices.length; i ++) {
            //今天不持有 就是
            //昨天持有今天卖出dp[i - 1][1] + prices[i],今天卖出的话就在昨天持有的基础上增加卖出得的钱
            //或则昨天已经卖出 dp[i - 1][0]
            dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] + prices[i]);
            //今天持有 就是
            //昨天卖出今天持有dp[i - 1][0] - prices[i],今天持有的话就在昨天未持有的基础上减去卖入出的钱
            //或则昨天持有今天继续持有 dp[i - 1][1]
            dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
        }
        return dp[prices.length - 1][0];
    }

    public static void main(String[] args) {
        System.out.println(maxProfitBigCoding(new int[]{1,2,3,4,5}));
    }
}
