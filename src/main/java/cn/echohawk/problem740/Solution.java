package cn.echohawk.problem740;

import java.util.*;

/**
 * @description: 给你一个整数数组 nums ，你可以对它进行一些操作。
 * 每次操作中，选择任意一个 nums[i] ，删除它并获得 nums[i] 的点数。
 * 之后，你必须删除每个等于 nums[i] - 1 或 nums[i] + 1 的元素。
 * 开始你拥有 0 个点数。返回你能通过这些操作获得的最大点数
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/delete-and-earn
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-05 09:59
 */
public class Solution {
	public static int deleteAndEarn(int[] nums) {
		int result = 0;
		Map<Integer, Integer> map = new TreeMap<>();
		map.put(0, 0);
		for (int num : nums) {
			Integer integer = map.get(num);
			if (integer == null) {
				integer = 0;
			}
			map.put(num, integer + num);
		}
		List<Integer> sumList = new ArrayList<>();
		sumList.add(0);
		sumList.add(0);
		sumList.add(0);
		int fontIndex = 0;
		for (Map.Entry<Integer, Integer> integerIntegerEntry : map.entrySet()) {
			Integer add = sumList.get(sumList.size() - 2);
			if (integerIntegerEntry.getKey() - 1 == fontIndex) {
				add = Math.max(add, sumList.get(sumList.size() - 3));
			} else {
				add = Math.max(add, sumList.get(sumList.size() - 1));
			}
			int i = add + integerIntegerEntry.getValue();
			result = Math.max(result, i);
			fontIndex = integerIntegerEntry.getKey();
			sumList.add(result);
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(deleteAndEarn(new int[]{3,4,2}));
	}
}
