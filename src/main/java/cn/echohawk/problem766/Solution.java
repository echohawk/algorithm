package cn.echohawk.problem766;

/**
 * @description: 托普斯矩阵
 * @author: echohawk
 * @create: 2021-06-08 17:13
 */
public class Solution {
	public boolean isToeplitzMatrix(int[][] matrix) {
		for (int i = 1; i < matrix.length; i++) {
			for (int j = 1; j < matrix[0].length; j++) {
				if (matrix[i][j] != matrix[i - 1][j - 1]) {
					return false;
				}
			}
		}
		return true;
	}
}
