package cn.echohawk.problem213;

/**
 * @description: 你是一个专业的小偷，计划偷窃沿街的房屋，每间房内都藏有一定的现金。这个地方所有的房屋都 围成一圈
 * ，这意味着第一个房屋和最后一个房屋是紧挨着的。同时，相邻的房屋装有相互连通的防盗系统，
 * 如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警 。  给定一个代表每个房屋存放金额的非负整数数组，
 * 计算你 在不触动警报装置的情况下 ，能够偷窃到的最高金额。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/house-robber-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-15 11:19
 */
public class Solution {
	public static int rob(int[] nums) {
		if (nums.length < 5) {
			if (nums.length == 3) {
				return Math.max(nums[1], Math.max(nums[0], nums[2]));
			} else if (nums.length == 4) {
				return Math.max(nums[0] + nums[2], nums[1] + nums[3]);
			} else if (nums.length == 1) {
				return nums[0];
			} else {
				return Math.max(nums[0], nums[1]);
			}
		}
		int result = 0;
		int[] dp = new int[nums.length];
		dp[0] = nums[0];
		dp[1] = nums[1];
		dp[2] = nums[0] + nums[2];
		for (int i = 3; i < nums.length - 1; i ++) {
			dp[i] = Math.max(dp[i - 2], dp[i - 3]) + nums[i];
		}
		result = Math.max(dp[nums.length - 2], dp[nums.length - 3]);
		dp[2] = nums[2];
		dp[3] = nums[1] + nums[3];
		for (int i = 4; i < nums.length; i ++) {
			dp[i] = Math.max(dp[i - 2], dp[i - 3]) + nums[i];
		}
		result = Math.max(Math.max(dp[nums.length - 1], dp[nums.length - 2]), result);
		return result;
	}

	public static void main(String[] args) {
		System.out.println(rob(new int[]{200,3,140,20,10}));
	}
}
