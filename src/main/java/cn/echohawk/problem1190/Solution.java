package cn.echohawk.problem1190;

import com.alibaba.fastjson.JSON;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * @description: 给出一个字符串 s（仅含有小写英文字母和括号）。
 * 请你按照从括号内到外的顺序，逐层反转每对匹配括号中的字符串，并返回最终的结果。
 * 注意，您的结果中 不应 包含任何括号。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/reverse-substrings-between-each-pair-of-parentheses
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-26 12:52
 */
public class Solution {
	public static String reverseParentheses(String s) {
		Deque<Character> deque = new LinkedList<Character>();
		Deque<Character> helpDeque = new LinkedList<Character>();
		char[] chars = s.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (')' == chars[i]) {
				while (!deque.isEmpty()) {
					Character pop = deque.pop();
					if (pop == '(') {
						break;
					}
					helpDeque.push(pop);
				}
				while (!helpDeque.isEmpty()) {
					deque.push(helpDeque.pollLast());
				}
			} else {
				deque.push(chars[i]);
			}
		}
		StringBuilder sb = new StringBuilder();
		while (!deque.isEmpty()) {
			sb.append(deque.pollLast());
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(reverseParentheses("(ed(et(oc))el)"));
	}
}
