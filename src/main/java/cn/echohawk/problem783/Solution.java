package cn.echohawk.problem783;

import javax.swing.tree.TreeNode;

/**
 * @description: 给你一个二叉搜索树的根节点 root ，返回 树中任意两不同节点值之间的最小差值 。
 * @author: echohawk
 * @create: 2021-04-13 10:28
 */
public class Solution {
	static int result = Integer.MAX_VALUE;
	static int tempNodeVal = Integer.MAX_VALUE;
	public static int minDiffInBST(TreeNode root) {
		if (root == null || root.left == null && root.right == null) {
			return 0;
		}
		deepLoop(root);
		return result;
	}

	private static void deepLoop(TreeNode root) {
		if (root == null) {
			return;
		}
		deepLoop(root.right);
		if (tempNodeVal != Integer.MAX_VALUE) {
			result = Math.min(tempNodeVal - root.val, result);
		}
		tempNodeVal = root.val;
		deepLoop(root.left);
	}

	class TreeNode {
 	    int val;
 	    TreeNode left;
 	    TreeNode right;
 	    TreeNode() {}
 	    TreeNode(int val) { this.val = val; }
 	    TreeNode(int val, TreeNode left, TreeNode right) {
 	        this.val = val;
 	        this.left = left;
 	        this.right = right;
 	    }
 	}

}
