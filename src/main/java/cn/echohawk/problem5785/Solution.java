package cn.echohawk.problem5785;

public class Solution {
    public String largestOddNumber(String num) {
        char[] chars = num.toCharArray();
        for (int i = chars.length - 1; i >= 0; i--) {
            if (chars[i] % 2 != 0) {
                return num.substring(0, i + 1);
            }
        }
        return "";
    }
    
}
