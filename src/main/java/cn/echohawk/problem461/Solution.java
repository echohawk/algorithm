package cn.echohawk.problem461;

/**
 * @description: 两个整数之间的汉明距离指的是这两个数字对应二进制位不同的位置的数目。
 * 给出两个整数 x 和 y，计算它们之间的汉明距离。
 * @author: echohawk
 * @create: 2021-05-27 12:10
 */
public class Solution {
	public int hammingDistance(int x, int y) {
		int result = x ^ y;
		result = (result & 0x55555555) + (result >>> 1 & 0x55555555);
		result = (result & 0x33333333) + (result >>> 2 & 0x33333333);
		result = (result & 0x0f0f0f0f) + (result >>> 4 & 0x0f0f0f0f);
		result = (result & 0x00ff00ff) + (result >>> 8 & 0x00ff00ff);
		result = (result & 0x0000ffff) + (result >>> 16 & 0x00ff00ff);
		return result;
	}
}
