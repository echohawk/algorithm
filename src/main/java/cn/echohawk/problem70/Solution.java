package cn.echohawk.problem70;

/**
 * @description: 假设你正在爬楼梯。需要 n 阶你才能到达楼顶。  每次你可以爬 1 或 2 个台阶。
 * 你有多少种不同的方法可以爬到楼顶呢？  注意：给定 n 是一个正整数。
 * @author: echohawk
 * @create: 2021-04-08 19:22
 */
public class Solution {
	public static int climbStairs(int n) {
		int[] dp = new int[]{1,2,0};
		int mark = n < 3 ? n - 1 : 1;
		for (int i = 2; i < n; i++) {
			mark++;
			if (mark == 3) {
				mark = 0;
			}
			dp[mark] = dp[mark-1>-1 ? mark-1 : mark+2] + dp[mark-2>-1 ? mark-2 : mark+1];
		}
		return dp[mark];
	}

	public static void main(String[] args) {
		System.out.println(climbStairs(4));
	}
}
