package cn.echohawk.problem5781;

import com.alibaba.fastjson.JSON;

public class Solution {
    public static int largestMagicSquare(int[][] grid) {
        int result = 1;
        int[][] sum = new int[grid.length + 1][grid[0].length + 1];
        int[][] sumY = new int[grid.length + 1][grid[0].length + 1];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                sum[i + 1][j + 1] = sum[i + 1][j] + grid[i][j];
                sumY[i + 1][j + 1] = sumY[i][j + 1] + grid[i][j];
            }
        }
        System.out.println(JSON.toJSONString(sum));
        System.out.println(JSON.toJSONString(sumY));
        for (int i = 0; i < grid.length; i++) {
            a:for (int j = 0; j < grid[i].length; j++) {
                System.out.println();
                for (int k = 1; k < grid.length - i && j + k < grid[i].length; k ++) {
                    int sumC = -1;
                    for (int i1 = 0; i1 <= k; i1++) {
                        System.out.println(" " + (i + 1 + i1) + " " + (j + 1 + k) + " " + (i + 1 + i1) + " " + j);
                        System.out.println(" " + (i + 1 + k) + " " + (j + 1 + i1) + " " + i + " " + (j + 1 + i1));
                        System.out.println(sum[i + 1 + i1][j + 1 + k] - sum[i + 1 + i1][j]);
                        System.out.println(sumY[i + 1 + k][j + 1 + i1] - sumY[i][j + 1 + i1]);
                        if (sumC == -1) {
                            sumC = sum[i + 1 + i1][j + 1 + k] - sum[i + 1 + i1][j];
                        }
                        if (sumC != sum[i + 1 + i1][j + 1 + k] - sum[i + 1 + i1][j]){
                            continue a;
                        }
                        if (sumY[i + 1 + k][j + 1 + i1] - sumY[i][j + 1 + i1] != sumC) {
                            continue a;
                        }
                    }
                    System.out.println("-------------" + sumC);
                    int sumA = 0, sumB = 0;
                    for (int i1 = 0; i1 <= k; i1++) {
                        sumA += grid[i + i1][j + i1];
                        sumB += grid[i + k - i1][j + i1];
                    }
                    if (sumA == sumB && sumB == sumC) {
                        result = Math.max(result, k + 1);
                    }
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(largestMagicSquare(new int[][]{{7,1,4,5,6},{2,5,1,6,4},{1,5,4,3,2},{1,2,7,3,4}}));
    }
}
