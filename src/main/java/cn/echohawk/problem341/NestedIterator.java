package cn.echohawk.problem341;

import java.util.*;

public class NestedIterator implements Iterator<Integer> {

    Integer next = null;
    Deque<NestedInteger> deque = new ArrayDeque<>();

    public NestedIterator(List<NestedInteger> nestedList) {
        if (nestedList != null) {
            for (NestedInteger nestedInteger : nestedList) {
                deque.addLast(nestedInteger);
            }
            next = getNext();
        }
    }

    private Integer getNext() {
        try {
            while (!deque.getFirst().isInteger()) {
                NestedInteger poll = deque.pollFirst();
                List<NestedInteger> list = poll.getList();
                for (int size = list.size() - 1; size >= 0; size--) {
                    deque.addFirst(list.get(size));
                }
            }
            return deque.pollFirst().getInteger();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Integer next() {
        Integer temp = next;
        next = getNext();
        return temp;
    }

    @Override
    public boolean hasNext() {
        return next != null;
    }
}

