package cn.echohawk.problem341;

import java.util.List;

public interface NestedInteger {
public boolean isInteger();
public Integer getInteger();
public List<NestedInteger> getList();
}
