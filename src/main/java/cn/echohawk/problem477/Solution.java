package cn.echohawk.problem477;

/**
 * @description: 两个整数的 汉明距离 指的是这两个数字的二进制数对应位不同的数量。
 * 计算一个数组中，任意两个数之间汉明距离的总和。
 * @author: echohawk
 * @create: 2021-05-28 12:03
 */
public class Solution {
	public int totalHammingDistance(int[] nums) {
		int[][] bucket = new int[32][2];
		int result = 0;
		for (int num : nums) {
			for (int i = 0; i < bucket.length; i++) {
				bucket[i][(num >>> i) & 0x01] += 1;
				result += bucket[i][(~(num >>> i)) & 0x01];
			}
		}
		return result;
	}
}
