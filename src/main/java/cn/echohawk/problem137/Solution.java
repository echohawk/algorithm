package cn.echohawk.problem137;

/**
 * 给你一个整数数组 nums ，除某个元素仅出现 一次 外，其余每个元素都恰出现 三次 。请你找出并返回那个只出现了一次的元素。
 */
public class Solution {
    public static int singleNumber(int[] nums) {
        int a = 0, b = 0;
        for (int num : nums) {
            int tempA = a & ~b & ~num | ~a & b & num;
            b = ~a & ~b & num | ~a & b & ~num;
            a = tempA;
        }
        return a;
    }

    public static void main(String[] args) {
        System.out.println(singleNumber(new int[]{2,2}));
    }
}
