package cn.echohawk.problem53;

/**
 * @description: 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 * @author: echohawk
 * @create: 2021-04-07 17:24
 */
public class Solution {
	public int maxSubArray(int[] nums) {
		int res = nums[0];
		int sum = 0;
		for (int num : nums) {
			if(sum < 0) {
				sum = num;
			} else {
				sum += num;
			}
			res = Math.max(sum, res);
		}
		return res;
	}
}
