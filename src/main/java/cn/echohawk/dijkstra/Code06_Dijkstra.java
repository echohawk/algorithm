package cn.echohawk.dijkstra;


import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Code06_Dijkstra {

    //初级Dijkstra 算法  通过类似动态规划的方法找最小路径
    public static Map<Node, Integer> dijkstra(Node head) {
        Map<Node, Integer> result = new HashMap<>();
        Map<Node, Integer> dp = new HashMap<>();
        dp.put(head, 0);
        while (head != null && !head.edges.isEmpty()) {
            Integer base = dp.get(head);
            for (Edge next : head.edges) {
                if (!result.containsKey(next.from)) {
                    dp.put(next.to, Math.min(base + next.weight, dp.getOrDefault(next.to, Integer.MAX_VALUE)));
                }
            }
            result.put(head, dp.get(head));
            dp.remove(head);
            //找寻剩余下面的最小的节点
            if (dp.isEmpty()) break;
            head = dp.entrySet().stream().min(Comparator.comparingInt(entry -> entry.getValue())).map(Map.Entry::getKey).get();
        }
        return result;
    }


    //Dijkstra 算法优化 优化部分在于找剩余node的最小node
    public static class NodeHeap {
        private Node[] nodes;
        private Map<Node, Integer> heapIndexMap;
        private Map<Node, Integer> distanceMap;
        private int size;

        public NodeHeap(int size) {
            nodes = new Node[size];
            heapIndexMap = new HashMap<>();
            distanceMap = new HashMap<>();
            this.size = 0;
        }

        public void addOrUpdateOrIgnore(Node node, int distance) {
            //如果堆中存在那就是更新操作
            if (inHeap(node)) {
                distanceMap.put(node, Math.min(distanceMap.get(node), distance));
                insertHeapify(node, heapIndexMap.get(node));
            }
            // 不曾进入就是单纯的添加
            if (!heapIndexMap.containsKey(node)) {
                nodes[size] = node;
                heapIndexMap.put(node, size);
                distanceMap.put(node, distance);
                insertHeapify(node, size++);
            }
        }

        public NodeRecord pop() {
            NodeRecord pop = new NodeRecord(nodes[0], distanceMap.get(nodes[0]));
            swap(0, size-1);
            heapIndexMap.put(nodes[size - 1], -1);
            distanceMap.remove(nodes[size - 1]);
            nodes[size - 1] = null;
            heapify(0, size--);
            return pop;
        }

        private void heapify(int start, int size) {
            int left = start * 2 + 1;
            while (left < size) {
                int small = left + 1 >= size || distanceMap.get(nodes[left]) < distanceMap.get(nodes[left + 1]) ? left : left+1;
                small = distanceMap.get(nodes[start]) < distanceMap.get(nodes[small]) ? start : small;
                if (start == small) break;
                swap(start, small);
                start = small;
                left = start * 2 + 1;
            }
        }

        private void insertHeapify(Node node, Integer index) {
            while (index > 0 && nodes[(index-1)/2].value > nodes[index].value) {
                swap((index-1)/2, index);
                index = (index-1)/2;
            }
        }

        private void swap(int i, Integer index) {
            Node temp = nodes[i];
            nodes[i] = nodes[index];
            nodes[index] = temp;
            heapIndexMap.put(nodes[index], index);
            heapIndexMap.put(nodes[i], i);
        }

        public boolean inHeap(Node node) {
            return heapIndexMap.getOrDefault(node, -1) != -1;
        }
    }

    public static class NodeRecord {
        public Node node;
        public int distance;

        public NodeRecord(Node node, int distance) {
            this.node = node;
            this.distance = distance;
        }
    }
}
