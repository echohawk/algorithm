package cn.echohawk.problem87;

/**
 * @description: 使用下面描述的算法可以扰乱字符串 s 得到字符串 t ： 如果字符串的长度为 1
 * ，算法停止 如果字符串的长度 > 1 ，执行下述步骤：
 * 在一个随机下标处将字符串分割成两个非空的子字符串。即，如果已知字符串 s ，
 * 则可以将其分成两个子字符串 x 和 y ，且满足 s = x + y 。
 * 随机 决定是要「交换两个子字符串」还是要「保持这两个子字符串的顺序不变」。
 * 即，在执行这一步骤之后，s 可能是 s = x + y 或者 s = y + x 。
 * 在 x 和 y 这两个子字符串上继续从步骤 1 开始递归执行此算法。
 * 给你两个 长度相等 的字符串 s1 和 s2，判断 s2 是否是 s1 的扰乱字符串。如果是，返回 true ；否则，返回 false 。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/scramble-string
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-16 10:26
 */
public class Solution {
	public boolean isScramble(String s1, String s2) {
		//看过题解之后进行DP
		char[] c1 = s1.toCharArray();
		char[] c2 = s2.toCharArray();
		int length = s1.length();
		boolean[][][] dp = new boolean[length][length][length + 1];
		//循环DP固定值
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < length; j ++) {
				dp[i][j][1] = (c1[i] == c2[j]);
			}
		}
		//循环长度
		for (int len = 2; len <= length; len++) {
			for (int x = 0; x < length-len; x++) {
				for (int y = 0; y < length-len; y ++) {
					for (int w = 1; w < len; w ++) {
						dp[x][y][len] = (dp[x][y][w] && dp[x + w][y + w][len - w]) || (dp[x][y + len - w][w] && dp[x + w][y][len - w]);
						if (dp[x][y][len]) {
							break;
						}
					}
				}
			}
		}
		return dp[0][0][length];
	}
}
