package cn.echohawk.problem331;

import com.alibaba.fastjson.JSON;

import java.util.Stack;

/**
 * @description: 序列化二叉树的一种方法是使用前序遍历。当我们遇到一个非空节点时，我们可以记录下这个节点的值。如果它是一个空节点，我们可以使用一个标记值记录，例如 #。
 * @author: echohawk
 * @create: 2021-06-02 12:18
 */
public class Solution {
	public static boolean isValidSerialization(String preorder) {
		Stack<String> stack = new Stack<>();
		String[] nodes = preorder.split(",");
		stack.push(nodes[0]);
		for (int i = 1; i < nodes.length; i++) {
			System.out.println(JSON.toJSONString(stack));
			if(stack.isEmpty()) {
				return false;
			}
			stack.push(nodes[i]);
			if("#".equals(stack.peek())) {
				try {
					while ("#".equals(stack.peek()) && stack.size() != 1) {
						stack.pop();
						if ("#".equals(stack.peek())) {
							stack.pop();
							stack.pop();
							stack.push("#");
						} else {
							stack.push("#");
							break;
						}
					}
				} catch (Exception e) {
					return false;
				}
			}
		}
		return stack.size() == 1 && "#".equals(stack.peek());
	}

	public static boolean isValidSerializationDoublePoint(String preorder) {
		if("#".equals(preorder)) return true;
		String[] nodes = preorder.split(",");
		int left = 0;
		for (int i = 0; i < nodes.length; i++) {
			System.out.println(JSON.toJSONString(nodes) + " " + left + " " + nodes[i]);
			if ("#".equals(nodes[i])) {
				if (left - 1 < 0) {
					return false;
				}
				while (left - 1 >= 0 && "#".equals(nodes[left - 1])) {
					left -= 2;
					if (left < 0 || "#".equals(nodes[left])) {
						return false;
					}
				}
			}
			nodes[left++] = nodes[i];
		}
		return left == 1 && "#".equals(nodes[0]);
	}

	public static void main(String[] args) {
		System.out.println(isValidSerializationDoublePoint("1,#"));
	}

}
