package cn.echohawk.problem1074;

import com.alibaba.fastjson.JSON;

/**
 * 给出矩阵 matrix 和目标值 target，返回元素总和等于目标值的非空子矩阵的数量。
 *
 * 子矩阵 x1, y1, x2, y2 是满足 x1 <= x <= x2 且 y1 <= y <= y2 的所有单元 matrix[x][y] 的集合。
 *
 * 如果 (x1, y1, x2, y2) 和 (x1', y1', x2', y2') 两个子矩阵中部分坐标不同（如：x1 != x1'），那么这两个子矩阵也不同。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/number-of-submatrices-that-sum-to-target
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static int numSubmatrixSumTarget(int[][] matrix, int target) {
        int result = 0;
        int[][] dp = new int[matrix.length + 1][matrix[0].length + 1];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                dp[i + 1][j + 1] = dp[i + 1][j] + dp[i][j + 1] - dp[i][j] + matrix[i][j];
                for (int x = 0; x < i + 1; x++) {
                    for (int y = 0; y < j + 1; y++) {
                        if (dp[i + 1][j + 1] - dp[i + 1][y] - dp[x][j + 1] + dp[x][y] == target) {
                            result++;
                        }
                    }
                }
            }
        }
        System.out.println(JSON.toJSONString(dp));
        return result;
    }

    public static void main(String[] args) {
        System.out.println(numSubmatrixSumTarget(new int[][]{{1,-1},{-1,1}}, 0));
    }
}
