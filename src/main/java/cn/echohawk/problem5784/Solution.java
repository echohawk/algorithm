package cn.echohawk.problem5784;

public class Solution {
    public boolean mergeTriplets(int[][] triplets, int[] target) {
        boolean[] flag = new boolean[3];
        for (int[] triplet : triplets) {
            if (triplet[0] <= target[0] && triplet[1] <= target[1] &&triplet[2] <= target[2]) {
                if (triplet[0] == target[0]) flag[0] = true;
                if (triplet[1] == target[1]) flag[1] = true;
                if (triplet[2] == target[2]) flag[2] = true;
            }
        }
        return flag[0] && flag[1] &&flag[2];
    }
}
