package cn.echohawk.problem82;

import cn.echohawk.problem61.ListNode;

import java.util.List;

/**
 * @description: 存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除链表中所有存在数字重复情况的节点，
 * 只保留原始链表中 没有重复出现 的数字。  返回同样按升序排列的结果链表。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-05 11:43
 */
public class Solution {
	public ListNode deleteDuplicates(ListNode head) {
		ListNode node = new ListNode(-101, head);
		ListNode frontNode = node;
		ListNode nowNode = node.next;
		while (nowNode != null) {
			if (nowNode.next != null && nowNode.val == nowNode.next.val) {
				while (nowNode.next != null && nowNode.val == nowNode.next.val) {
					nowNode = nowNode.next;
				}
				nowNode = nowNode.next;
				frontNode.next = nowNode;
			} else {
				frontNode = nowNode;
				nowNode = nowNode.next;
			}
		}
		return node.next;
	}
}
