package cn.echohawk.problem455;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @description: 假设你是一位很棒的家长，想要给你的孩子们一些小饼干。但是，每个孩子最多只能给一块饼干。
 * 对每个孩子 i，都有一个胃口值 g[i]，这是能让孩子们满足胃口的饼干的最小尺寸；并且每块饼干 j，都有一个尺寸 s[j] 。
 * 如果 s[j] >= g[i]，我们可以将这个饼干 j 分配给孩子 i ，这个孩子会得到满足。
 * 你的目标是尽可能满足越多数量的孩子，并输出这个最大数值。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/assign-cookies
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-19 12:19
 */
public class Solution {

	//o(n2)
	public static int findContentChildren(int[] g, int[] s) {
		int result = 0;
		for (int cookie : s) {
			int maxIndex = -1;
			for(int i = 0; i < g.length; i++) {
				if (g[i] > 0 && g[i] <= cookie) {
					if (maxIndex == -1) {
						maxIndex = i;
					} else if (g[maxIndex] < g[i]){
						maxIndex = i;
					}
				}
			}
			if (maxIndex != -1) {
				result++;
				g[maxIndex] = -1;
			}
		}
		return result;
	}

	public static int betterFindContentChildren(int[] g, int[] s) {
		int result = 0;
		Arrays.sort(s);
		Arrays.sort(g);
		int gIn = g.length - 1;
		out:for (int sIn = s.length - 1; gIn >= 0 && sIn >= 0; sIn --) {
			while(s[sIn] < g[gIn]) {
				gIn --;
				if (gIn < 0) {
					break out;
				}
			}
			result++;
			gIn--;
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(betterFindContentChildren(new int[]{1,2}, new int[]{1,2,3}));
	}
}
