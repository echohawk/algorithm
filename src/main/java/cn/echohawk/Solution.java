package cn.echohawk;

/**
 * @description: 1049. 最后一块石头的重量 II
 * @author: echohawk
 * @create: 2021-06-08 13:56
 */
public class Solution {
	public int lastStoneWeightII(int[] stones) {
		int sum = 0;
		for (int stone : stones) {
			sum += stone;
		}
		int[] dp = new int[sum/2 + 1];
		for (int stone : stones) {
			for (int i = dp.length - 1; i >= stone; i--) {
				dp[i] = Math.max(dp[i], dp[i - stone] + stone);
			}
		}
		return sum - 2*dp[sum/2];
	}
}
