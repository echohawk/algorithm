package cn.echohawk.problem190;

import java.util.Collection;

/**
 * @description: 颠倒给定的 32 位无符号整数的二进制位。
 * @author: echohawk
 * @create: 2021-05-03 10:47
 */
public class Solution {
	public static int reverseBits(int n) {
		n = (n & 0x55555555) << 1 | (n >>> 1) & 0x55555555;
		n = (n & 0x33333333) << 2 | n >>> 2 & 0x33333333;
		n = (n & 0xf0f0f0f) << 4 | n >>> 4 & 0xf0f0f0f;
		return n << 24 | (n << 8) & 0xff0000 | (n >>> 8) & 0xff00 | n >>> 24;
	}

	public static void main(String[] args) {
//		System.out.println(reverseBits(2147483525));
	}
}
