package cn.echohawk.problem897;

/**
 * @description: 给你一棵二叉搜索树，请你 按中序遍历 将其重新排列为一棵递增顺序搜索树，
 * 使树中最左边的节点成为树的根节点，并且每个节点没有左子节点，只有一个右子节点。
 * @author: echohawk
 * @create: 2021-04-25 11:07
 */
public class Solution {
	private TreeNode firstNode = new TreeNode();
	private TreeNode tempNode = firstNode;
	public TreeNode increasingBST(TreeNode root) {
		deepBST(root);
		tempNode.left = tempNode.right = null;
		return firstNode.right;
	}
	public void deepBST(TreeNode root) {
		if (root == null) return;
		deepBST(root.left);
		tempNode.right = root;
		tempNode.left = null;
		tempNode = root;
		deepBST(root.right);
	}
}
