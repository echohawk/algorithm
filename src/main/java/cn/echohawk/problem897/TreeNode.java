package cn.echohawk.problem897;

/**
 * @description: treeNode
 * @author: echohawk
 * @create: 2021-04-25 11:09
 */
public class TreeNode {
     public int val;
     public TreeNode left;
     public TreeNode right;
     public TreeNode() {}
	 public TreeNode(int val) { this.val = val; }
	 public TreeNode(int val, TreeNode left, TreeNode right) {
     	this.val = val;
     	this.left = left;
     	this.right = right;
     }
}
