package cn.echohawk.problem83;

import cn.echohawk.problem61.ListNode;

/**
 * @description: 存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除所有重复的元素，使每个元素 只出现一次 。  返回同样按升序排列的结果链表。
 * @author: echohawk
 * @create: 2021-05-05 11:13
 */
public class Solution {
	public ListNode deleteDuplicates(ListNode head) {
		if (head == null) {
			return head;
		}
		int index = head.val;
		ListNode next = head.next;
		ListNode temp = head;
		head.next = null;
		while (next != null) {
			if (index != next.val) {
				temp.next = next;
				temp = next;
				index = next.val;
				next = next.next;
				temp.next = null;
			} else {
				next = next.next;
			}
		}
		return head;
	}


	public ListNode deleteDuplicatesSe(ListNode head) {
		if (head == null) {
			return head;
		}
		ListNode next = head.next;
		ListNode temp = head;
		while (next != null) {
			if (temp.val != next.val) {
				temp.next = next;
				temp = next;
			}
			next = next.next;
		}
		temp.next = null;
		return head;
	}

	public ListNode deleteDuplicatesTh(ListNode head) {
		ListNode node = head;
		while (node != null) {
			if (node.next != null && node.val == node.next.val) {
				node.next = node.next.next;
			} else {
				node = node.next;
			}
		}
		return head;
	}

}
