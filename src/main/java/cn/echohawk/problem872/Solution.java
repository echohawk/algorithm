package cn.echohawk.problem872;

import cn.echohawk.problem897.TreeNode;

import java.util.Stack;

/**
 * @description: 请考虑一棵二叉树上所有的叶子，这些叶子的值按从左到右的顺序排列形成一个 叶值序列 。
 * @author: echohawk
 * @create: 2021-05-10 09:23
 */
public class Solution {
	public boolean leafSimilar(TreeNode root1, TreeNode root2) {
		Stack<TreeNode> stack1 = new Stack<>();
		Stack<TreeNode> stack2 = new Stack<>();
		TreeNode temp = new TreeNode(Integer.MAX_VALUE);
		TreeNode compareNode = temp;
		int leafNode1=0, leafNode2=0;
		while(true) {
			while (!stack1.isEmpty() || root1 != null) {
				if (root1 == null) {
					TreeNode pop = stack1.pop();
					root1 = pop.right;
					if(pop.left == null && pop.right == null) {
						compareNode = pop;
						leafNode1++;
						break;
					}
				} else {
					stack1.push(root1);
					root1 = root1.left;
				}
			}
			while (!stack2.isEmpty() || root2 != null) {
				if (root2 == null) {
					TreeNode pop = stack2.pop();
					root2 = pop.right;
					if(pop.left == null && pop.right == null) {
						if (compareNode.val != pop.val) {
							return false;
						}
						leafNode2++;
						compareNode = temp;
						break;
					}
				} else {
					stack2.push(root2);
					root2 = root2.left;
				}
			}
			if (stack1.isEmpty() && stack2.isEmpty()) {
				break;
			}
		}
		if (leafNode1 == leafNode2) {
			return true;
		}
		return false;

	}
}
