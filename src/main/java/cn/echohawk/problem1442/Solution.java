package cn.echohawk.problem1442;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 给你一个整数数组 arr 。  现需要从数组中取三个下标 i、j 和 k ，
 * 其中 (0 <= i < j <= k < arr.length) 。  a 和 b 定义如下：
 * a = arr[i] ^ arr[i + 1] ^ ... ^ arr[j - 1] b = arr[j] ^ arr[j + 1] ^ ... ^ arr[k]
 * 注意：^ 表示 按位异或 操作。
 * 请返回能够令 a == b 成立的三元组 (i, j , k) 的数目。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/count-triplets-that-can-form-two-arrays-of-equal-xor
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-18 11:03
 */
public class Solution {
	public int countTriplets(int[] arr) {
		Map<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();
		int result = 0;
		List<Integer> init = map.getOrDefault(0, new ArrayList<>());
		init.add(-1);
		map.put(0, init);
		int sumXOR = 0;
		for (int i = 0; i < arr.length; i++) {
			sumXOR ^= arr[i];
			List<Integer> integers = map.getOrDefault(sumXOR, new ArrayList<>());
			for (Integer integer : integers) {
				result += (i - integer - 1);
			}
			integers.add(i);
			map.put(sumXOR, integers);
		}
		return result;
	}
}
