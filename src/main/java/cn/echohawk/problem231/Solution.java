package cn.echohawk.problem231;

/**
 * 给你一个整数 n，请你判断该整数是否是 2 的幂次方。如果是，返回 true ；否则，返回 false 。
 *
 * 如果存在一个整数 x 使得 n == 2x ，则认为 n 是 2 的幂次方。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/power-of-two
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static boolean isPowerOfTwo(int n) {
        if (n < 1) {
            return false;
        }
        n = ((n >>> 1) & 0x55555555) + (n & 0x55555555);
        n = ((n >>> 2) & 0x33333333) + (n & 0x33333333);
        n = ((n >>> 4)& 0xF0F0F0F) + (n & 0xF0F0F0F);
        n = ((n >>> 8) & 0xFF00FF) + (n & 0xFF00FF);
        n = ((n >>> 16) & 0xFFFF) + (n & 0xFFFF);
        return n > 0 && n == 1;
    }

    public static void main(String[] args) {
        System.out.println(isPowerOfTwo(4));
    }
}
