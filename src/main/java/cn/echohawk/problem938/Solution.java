package cn.echohawk.problem938;

import cn.echohawk.problem897.TreeNode;

/**
 * @description: 给定二叉搜索树的根结点 root，返回值位于范围 [low, high] 之间的所有结点的值的和。
 * @author: echohawk
 * @create: 2021-04-27 17:48
 */
public class Solution {
	int result = 0;
	public int rangeSumBST(TreeNode root, int low, int high) {
		deepBST(root, low, high);
		return result;
	}

	public void deepBST(TreeNode root, int low, int high) {
		if (root == null) return;
		deepBST(root.left, low, high);
		if (root.val >= low && root.val <= high) {
			result += root.val;
		}
		deepBST(root.right, low, high);
	}
}
