package cn.echohawk.problem5782;

public class Solution {
    public boolean makeEqual(String[] words) {
        int[] count = new int[26];
        for (String word : words) {
            for (char c : word.toCharArray()) {
                count[c - 'a'] += 1;
            }
        }
        for (int i : count) {
            if (i != 0 && i % words.length != 0) {
                return false;
            }
        }
        return true;
    }
}
