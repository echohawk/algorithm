package cn.echohawk.problem220;

/**
 * 给你一个整数数组 nums 和两个整数 k 和 t 。请你判断是否存在两个下标 i 和 j，使得 abs(nums[i] - nums[j]) <= t ，同时又满足 abs(i - j) <= k 。
 *
 * 如果存在则返回 true，不存在返回 false。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/contains-duplicate-iii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        int start = 0;
        for (int end = 1; end < nums.length; end ++) {
            if (end - start > k) {
                start++;
            }
            for (int comare = end - 1; comare > -1 && comare >= start; comare --) {
                int i = Math.min(nums[end], nums[comare]) + t;
                //溢出
                if (i < Math.min(nums[end], nums[comare])) {
                    return true;
                }
                if (i >= Math.max(nums[end], nums[comare])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(containsNearbyAlmostDuplicate(new int[]{1,0,1,1}, 1,2));
    }
}
