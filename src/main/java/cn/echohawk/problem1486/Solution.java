package cn.echohawk.problem1486;

/**
 * @description: 给你两个整数，n 和 start 。  数组 nums 定义为：nums[i] = start + 2*i（下标从 0 开始）
 * 且 n == nums.length 。  请返回 nums 中所有元素按位异或（XOR）后得到的结果。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/xor-operation-in-an-array
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-07 11:39
 */
public class Solution {

	//错误解答
	public int xorOperation(int n, int start) {
		int i = n % 4;
		int i1 = start % 4;
		if (i1 == 0 || i1 == 1) {
			if (i == 2 || i ==0) {
				return i;
			} else {
				return i == 1 ? start + (n/4) * 8 : start ^ (start + 2) ^ (start + 4) + (n/4) * 8;
			}
		} else {
			if (i == 1 || i == 3) {
				return i == 1 ? start : start ^ (start + 2) ^ (start + 4);
			} else {
				return i == 0 ? (start ^ (start + 2) + (n/4) * 8) : (start ^ (start + 2) ^(start + 4)^(start + 6) + (n/4) * 8);
			}
		}
	}

	public int xorOperationMath(int n, int start) {
		//start除二
		int s = start >> 1;
		int prefix = sumXOR(s - 1) ^ sumXOR(s + n - 1);
		int first = start & n & 1;
		return prefix << 1 | first;
	}

	public int sumXOR(int n) {
		int mod = n % 4;
		if (mod == 0) return n;
		if (mod == 1) return 1;
		if (mod == 2) return n + 1;
		return 0;
	}




public static void main(String[] args) {
	int temp = 0;
	int temp2 = 0;
	int temp1 = 0;
	int temp3 = 0;
	int temp4 = 0;
	int temp5 = 0;
	for (int i = 0; i < 20; i++) {
		temp ^= (2 * i);
		temp1 ^= (2 * i + 1);
		temp2 ^= (2 * i + 2);
		temp3 ^= (2 * i + 3);
		temp4 ^= (2 * i + 4);
		temp5 ^= (2 * i + 5);
		System.out.println(temp + " " + temp1 + " " + temp2 + " " + temp3+" " + temp4+ " " + temp5);
	}
}
}
