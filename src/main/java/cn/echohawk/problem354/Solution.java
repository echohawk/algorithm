package cn.echohawk.problem354;

import com.alibaba.fastjson.JSON;

import java.util.*;

public class Solution {
    public static int maxEnvelopes(int[][] envelopes) {
        Set<Integer> xSet = new HashSet<>();
        Set<Integer> ySet = new HashSet<>();
        List<int[]> envelopeList = new ArrayList<>();
        for (int[] envelope : envelopes) {
            xSet.add(envelope[0]);
            ySet.add(envelope[1]);
            envelopeList.add(envelope);
        }
        List<Integer> xlist = new ArrayList<>(xSet);
        List<Integer> ylist = new ArrayList<>(ySet);
        Collections.sort(xlist);
        Collections.sort(ylist);
        Collections.sort(envelopeList, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o2[0] == o1[0] ? o1[1] - o2[1] : o1[0] - o2[0];
            }
        });
        System.out.println(JSON.toJSONString(xlist));
        System.out.println(JSON.toJSONString(ylist));
        System.out.println(JSON.toJSONString(envelopeList));
        int result = 0;
        int envelopeListIndex = 0;
        int[][] dp = new int[xlist.size() + 1][ylist.size() + 1];
        for (int x = 1; x <= xlist.size(); x++) {
            for (int y = 1; y <= ylist.size(); y++) {
                if (envelopeListIndex < envelopeList.size()
                        && envelopeList.get(envelopeListIndex)[0] == xlist.get(x - 1)
                        && envelopeList.get(envelopeListIndex)[1] == ylist.get(y - 1)) {
                    dp[x][y] = dp[x - 1][y - 1] + 1;
                    result = Math.max(dp[x][y], result);
                    envelopeListIndex++;
                    while (envelopeListIndex < envelopeList.size()
                            && envelopeList.get(envelopeListIndex)[0] == xlist.get(x - 1)
                            && envelopeList.get(envelopeListIndex)[1] == ylist.get(y - 1)) {
                        envelopeListIndex++;
                    }
                } else {
                    dp[x][y] = Math.max(dp[x - 1][y], dp[x][y - 1]);
                }

            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(maxEnvelopes(new int[][] {{5,4},{6,4},{6,7},{2,3}}));
    }
}
