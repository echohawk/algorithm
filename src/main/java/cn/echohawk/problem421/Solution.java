package cn.echohawk.problem421;

/**
 * 给你一个整数数组 nums ，返回 nums[i] XOR nums[j] 的最大运算结果，其中 0 ≤ i ≤ j < n 。
 *
 * 进阶：你可以在 O(n) 的时间解决这个问题吗？
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/maximum-xor-of-two-numbers-in-an-array
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    class Node{
        Node[] tf = new Node[2];
    }
    Node root = new Node();
    public void add(int val) {
        Node temp = root;
        for (int i = 31; i >= 0; i--) {
            int index = (val >> i) & 0x01;
            if (temp.tf[index] == null) {
                temp.tf[index] = new Node();
            }
            temp = temp.tf[index];
        }
    }

    public int getVal(int val) {
        int returnVal = 0;
        Node temp = root;
        for (int i = 31; i >= 0; i--) {
            int index = ((val >> i) ^ 0x01) & 0x01;
            if (temp.tf[index] == null) {
                index = index == 1 ? 0 : 1;
            }
            returnVal = returnVal | (index << i);
            temp = temp.tf[index];
        }
        return returnVal;
    }

    public int findMaximumXOR(int[] nums) {
        int result = 0;
        for (int num : nums) {
            add(num);
            result = Math.max(result, num ^ getVal(num));
        }
        return result;
    }
}
