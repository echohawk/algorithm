package cn.echohawk.problem132;

/**
 * @description: 给你一个字符串 s，请你将 s 分割成一些子串，使每个子串都是回文。  返回符合要求的 最少分割次数 。
 * @author: echohawk
 * @create: 2021-06-04 17:25
 */
public class Solution {
	public int minCut(String s) {
		//处理是回文串的范围
		char[] chars = s.toCharArray();
		boolean[][] isRevert = new boolean[s.length()][s.length()];
		for (int x = s.length() - 1; x >= 0; x--) {
			for (int y = x; y < s.length(); y++) {
				if (x == y) {
					isRevert[x][y] = true;
					if (x + 1 < s.length()) isRevert[x + 1][y] = true;
				} else if (chars[x] == chars[y] && isRevert[x + 1][y - 1]){
					isRevert[x][y] = true;
				}
			}
		}
		//dp 处理最小解决方案
		int[] dp = new int[s.length() + 1];
		dp[1] = 1;
		for (int i = 2; i <= chars.length; i++) {
			dp[i] = dp[i - 1] + 1;
			for (int x = 0; x < i - 1; x++) {
				if (isRevert[x][i - 1]) {
					dp[i] = Math.min(dp[i], dp[x] + 1);
				}
			}
		}
		return dp[s.length()] - 1;
	}
}
