package cn.echohawk.problem474;

/**
 * 给你一个二进制字符串数组 strs 和两个整数 m 和 n 。
 *
 * 请你找出并返回 strs 的最大子集的大小，该子集中 最多 有 m 个 0 和 n 个 1 。
 *
 * 如果 x 的所有元素也是 y 的元素，集合 x 是集合 y 的 子集 。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/ones-and-zeroes
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public int findMaxForm(String[] strs, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];
        for (String str : strs) {
            int num0 = 0, num1 = 0;
            for (char c : str.toCharArray()) {
                if (c == '0') num0 ++;
                else num1++;
            }
            for (int x = m; x >= num0; x--) {
                for (int y = n; y >= num1; y --) {
                    dp[x][y] = Math.max(dp[x][y], dp[x - num0][y - num1] + 1);
                }
            }
        }
        return dp[m][n];
    }
}
