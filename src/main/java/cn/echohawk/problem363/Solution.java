package cn.echohawk.problem363;

import com.alibaba.fastjson.JSON;

/**
 * @description: 给你一个 m x n 的矩阵 matrix 和一个整数 k ，找出并返回矩阵内部矩形区域的不超过 k 的最大数值和。
 * 题目数据保证总会存在一个数值和不超过 k 的矩形区域。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/max-sum-of-rectangle-no-larger-than-k
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-22 09:32
 */
public class Solution {

	/**
	 * 暴力DP 我无了
	 * @param matrix
	 * @param k
	 * @return
	 */
	public static int maxSumSubmatrix(int[][] matrix, int k) {
		int result = Integer.MAX_VALUE;
		int[][][][] dp = new int[matrix.length + 1][matrix[0].length + 1][matrix.length + 1][matrix[0].length + 1];
		for (int x = 0; x < matrix.length; x++) {
			for (int y = 0; y < matrix[0].length; y++) {
				for (int zx = 1; zx <= x + 1; zx ++) {
					for (int zy = 1; zy <= y + 1; zy ++) {
						dp[x + 1][y + 1][zx][zy] = dp[x][y + 1][zx-1][zy] + dp[x + 1][y][1][zy-1] + matrix[x][y];
						if (dp[x + 1][y + 1][zx][zy] <= k) {
							result = Math.min(k - dp[x + 1][y + 1][zx][zy], result);
						}
					}
				}
			}
		}
		return k - result;
	}


	/**
	 * 既然DP是数组过大那么 如果我用一列去存 这样就OK了 有思路了  先试x轴DP  之后可以选一个短的DP
	 * @param matrix
	 * @param k
	 * @return
	 */
	public static int betterMaxSumSubmatrix(int[][] matrix, int k) {
		int result = Integer.MAX_VALUE;
		int[][][] dp = new int[matrix.length + 1][matrix.length + 1][matrix[0].length + 1];
		for (int y = 0; y < matrix[0].length; y++) {
			for (int x = 0; x < matrix.length; x++) {
				int[][] temp = new int[matrix.length + 1][matrix[0].length + 1];
				for (int zx = 1; zx <= x + 1; zx ++) {
					for (int zy = 1; zy <= y + 1; zy ++) {
						temp[zx][zy] = dp[x][zx-1][zy] + dp[x + 1][1][zy-1] + matrix[x][y];
						if (temp[zx][zy] <= k) {
							result = Math.min(k - temp[zx][zy], result);
							if (result == 0) {
								return k;
							}
						}
					}
				}
				dp[x + 1] = temp;
			}
		}
		return k - result;
	}



	public static void main(String[] args) {
		System.out.println(maxSumSubmatrix(new int[][]{{2,2,-1}}, 3));
	}
}
