package cn.echohawk.problem1744;

import java.awt.*;

/**
 * @description:
 * @author: echohawk
 * @create: 2021-06-01 09:34
 */
public class Solution {
	public static boolean[] canEat(int[] candiesCount, int[][] queries) {
		int n = queries.length, m = candiesCount.length;
		boolean[] result = new boolean[n];
		long[] candie = new long[m + 1];
		for(int i = 0; i < m; i ++) {
			candie[i + 1] = candie[i] + candiesCount[i];
		}
		for(int i = 0; i < n; i ++) {
			long min = (candie[queries[i][0]] / queries[i][2]) + 1;
			long max = candie[queries[i][0] + 1];
			if (queries[i][1] + 1 >= min && max >= queries[i][1] + 1) {
				result[i] = true;
			}
		}
		return result;
	}

	public static void main(String[] args) {
//		System.out.println(canEat(new int[]{5,2,6,4,1},new int[][]{{3,1,2},{4,10,3},{3,10,100},{4,100,30},{1,3,1}}));
		int[] a = new int[]{46,5,47,48,43,34,15,26,11,25,41,47,15,25,16,50,32,42,32,21,36,34,50,45,46,15,46,38,50,12,3,26,26,16,23,1,4,48,47,32,47,16,33,23,38,2,19,50,6,19,29,3,27,12,6,22,33,28,7,10,12,8,13,24,21,38,43,26,35,18,34,3,14,48,50,34,38,4,50,26,5,35,11,2,35,9,11,31,36,20,21,37,18,34,34,10,21,8,5};
		int result = 0;
		for (int i = 0; i < 85; i++) {
			result+= a[i];
		}
		System.out.println(a[85]);
		System.out.println(result);
	}
}
