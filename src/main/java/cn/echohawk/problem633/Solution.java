package cn.echohawk.problem633;

/**
 * @description: 给定一个非负整数 c ，你要判断是否存在两个整数 a 和 b，使得 a2 + b2 = c 。
 * @author: echohawk
 * @create: 2021-04-28 12:01
 */
public class Solution {
	public static boolean judgeSquareSum(int c) {
		int min = 0;
		int max = (int)Math.sqrt(c);
		while (min <= max) {
			int value = min * min + max * max;
			if (value == c) {
				return true;
			} else if (value > c) {
				max --;
			} else {
				min ++;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println(judgeSquareSum(3));
	}

}
