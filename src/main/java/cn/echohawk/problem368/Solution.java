package cn.echohawk.problem368;

import java.lang.reflect.Parameter;
import java.util.*;

/**
 * @description: 给你一个由 无重复 正整数组成的集合 nums ，请你找出并返回其中最大的整除子集 answer ，
 * 子集中每一元素对 (answer[i], answer[j]) 都应当满足： answer[i] % answer[j] == 0 ，
 * 或 answer[j] % answer[i] == 0 如果存在多个有效解子集，返回其中任何一个均可。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/largest-divisible-subset
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-23 11:16
 */
public class Solution {
	static Map<Integer, List<Integer>> map = new HashMap<>();

	public static List<Integer> largestDivisibleSubset(int[] nums) {
		List<Integer> result = new ArrayList<>();
		if (nums.length == 1) {
			result.add(nums[0]);
			return result;
		}
		Arrays.sort(nums);
		int[] dp = new int[nums.length];
		for (int i = 1; i < nums.length; i++) {
			int half = nums[i] >> 1;
			int halfIndex = binarySearch(nums, half, 0, i);
			System.out.println(i + "--" + halfIndex);
			while(halfIndex > -1) {
				if (i < halfIndex) {
					halfIndex--;
					continue;
				}
				if (nums[i] % nums[halfIndex] == 0) {
					if (dp[halfIndex] == 0) {
						dp[halfIndex] = 1;
						addMap(1, nums[halfIndex]);
					}
					dp[i] = Math.max(dp[halfIndex] + 1, dp[i]);
					addMap(dp[i], nums[i]);
				}
				halfIndex--;
			}
		}
		if (map.size() == 0) {
			result.add(nums[0]);
			return result;
		}
		List<Integer> integers = map.get(map.size());
		result.add(integers.get(0));
		//挑一个输出
		for (int i = map.size() - 1; i > 0; i --) {
			List<Integer> integers1 = map.get(i);
			for (Integer integer : integers1) {
				if (result.get(result.size() - 1) % integer == 0 && !result.get(result.size() - 1).equals(integer)) {
					result.add(integer);
					break;
				}
			}
		}
		return result;
	}

	private static void addMap(Integer key, Integer addValue) {
		List<Integer> integers = map.get(key);
		if (integers == null) {
			integers = new ArrayList<>();
			map.put(key, integers);
		}
		integers.add(addValue);
	}

	private static int binarySearch(int[] nums, int target, int start, int end) {
		int mid = start + ((end - start) >> 1);
		int maxIndex = end;
		while (end > start) {
			if (nums[mid] > target) {
				maxIndex = mid;
				end = mid - 1;
			} else if (nums[mid] < target) {
				start = mid + 1;
			} else {
				return mid;
			}
			mid = start + ((end - start) >> 1);
		}
		return maxIndex;
	}

	public static void main(String[] args) {
		System.out.println(largestDivisibleSubset(new int[]{546,669}));
	}
}
