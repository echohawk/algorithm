package cn.echohawk.problem993;

import cn.echohawk.problem897.TreeNode;

/**
 * @description: 在二叉树中，根节点位于深度 0 处，每个深度为 k 的节点的子节点位于深度 k+1 处。
 * 如果二叉树的两个节点深度相同，但 父节点不同 ，则它们是一对堂兄弟节点。
 * 我们给出了具有唯一值的二叉树的根节点 root ，以及树中两个不同节点的值 x 和 y 。
 * 只有与值 x 和 y 对应的节点是堂兄弟节点时，才返回 true 。否则，返回 false。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/cousins-in-binary-tree
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-17 12:22
 */
public class Solution {
	int nodelevel = -1;
	int nodeValue = -1;
	int x,y;
	public boolean isCousins(TreeNode root, int _x, int _y) {
		x = _x;
		y = _y;
		return deep(0, root, -2);
	}

	private boolean deep(int deep, TreeNode node, int parent) {
		if (node == null) {
			return false;
		}
		boolean left = deep(deep + 1, node.left, node.val);
		if (node.val == x || node.val == y) {
			if (nodelevel != -1) {
				if (nodelevel == deep && nodeValue != parent) {
					return true;
				} else {
					return false;
				}
			}
			nodelevel = deep;
			nodeValue = parent;
		}
		boolean right = deep(deep + 1, node.right, node.val);
		return left | right;
	}
}
