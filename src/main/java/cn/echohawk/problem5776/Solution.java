package cn.echohawk.problem5776;

public class Solution {
    public boolean findRotation(int[][] mat, int[][] target) {
        boolean flag0 = true, flag90 = true, flag180 = true, flag270 = true;
        int l = mat.length;
        for (int x = 0; x < l; x++) {
            for (int y = 0; y < l; y++) {
                if (flag0 && target[x][y] != mat[x][y]) {
                    flag0 = false;
                }
                if (flag90 && target[x][y] != mat[y][l - x - 1]) {
                    flag90 = false;
                }
                if (flag180 && target[x][y] != mat[l - y - 1][x]) {
                    flag180 = false;
                }
                if (flag270 && target[x][y] != mat[l - x - 1][l - y - 1]) {
                    flag270 = false;
                }
            }
        }
        return flag0 || flag90 || flag180 || flag270;
    }

}
