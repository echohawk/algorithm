package cn.echohawk.problem1473;

/**
 * @description: 在一个小城市里，有 m 个房子排成一排，你需要给每个房子涂上 n 种颜色之一（颜色编号为 1 到 n ）。
 * 有的房子去年夏天已经涂过颜色了，所以这些房子不需要被重新涂色。  我们将连续相同颜色尽可能多的房子称为一个街区。
 * （比方说 houses = [1,2,2,3,3,2,1,1] ，它包含 5 个街区  [{1}, {2,2}, {3,3}, {2}, {1,1}] 。）
 * 给你一个数组 houses ，一个 m * n 的矩阵 cost 和一个整数 target ，
 * 其中：  houses[i]：是第 i 个房子的颜色，0 表示这个房子还没有被涂色。
 * cost[i][j]：是将第 i 个房子涂成颜色 j+1 的花费。 请你返回房子涂色方案的最小总花费，使得每个房子都被涂色后，恰好组成 target 个街区。
 * 如果没有可用的涂色方案，请返回 -1 。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/paint-house-iii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-04 10:40
 */
public class Solution {
	public static int minCost(int[] houses, int[][] cost, int m, int n, int target) {
		int result = -1;
		int[][][] dp = new int[m + 1][n + 1][target + 1];
		//第零层除了0其他的都设为-1
		for (int x = 0; x <= m; x ++) {
			for (int y = 0; y <= n; y++) {
				dp[x][y][0] = -1;
			}
		}
		for (int i = 1; i <= target; i ++) {
			for (int x = 0; x <= m; x ++) {
				for (int y = 0; y <= n; y++ ) {
					int min = -1;
					if (y == 0 || x < i) {
						if (y != 0 && x == i - 1 && (houses[x] == y || houses[x] == 0)) {
							dp[x][y][i] = 0;
						} else {
							dp[x][y][i] = min;
						}
						continue;
					}
					for (int z = 1; z <= n; z ++) {
						if (z == y && (houses[x - 1] == z || houses[x - 1] == 0)) {
							min = findMin(min, dp[x - 1][z][i]);
						} else if (z != y && (x != i || dp[x - 1][z][i - 1] != 0)) { //
							min = findMin(min, dp[x - 1][z][i - 1]);
						}
					}
					dp[x][y][i] = min != -1 ? (min + (houses[x - 1] != 0 ? 0 : cost[x - 1][y - 1])) : -1;
					if (i == target && x == m && dp[x][y][i] != -1) {
						if (result != -1) {
							result = Math.min(result, dp[x][y][i]);
						} else {
							result = dp[x][y][i];
						}
					}
				}
			}
		}
		for (int i = 0; i <= target; i++) {
			for (int x= 0; x <= m; x++) {
				System.out.println();
				for (int y = 0; y <= n; y++) {
					System.out.print(dp[x][y][i] + " ");
				}
			}
			System.out.println();
		}
		return result;
	}

	private static int findMin(int min, int comp) {
		if (min == 0) {
			if (comp != 0 && comp != -1) {
				return comp;
			}
			return min;
		} else if (min == -1) {
			return comp;
		} else {
			if (comp != 0 && comp != -1) {
				return Math.min(min, comp);
			}
			return min;
		}
	}


	public static int minCostDp(int[] houses, int[][] cost, int m, int n, int target) {
		int INF = 0x3f3f3f3f;
		int result = INF;
		int[][][] dp = new int[m + 1][n + 1][target + 1];
		//第零层除了0其他的都设为-1
		for (int x = 0; x <= m; x ++) {
			for (int y = 0; y <= n; y++) {
				dp[x][y][0] = INF;
			}
		}
		for (int i = 1; i <= target; i ++) {
			for (int x = 1; x <= m; x ++) {
				for (int y = 1; y <= n; y++ ) {
					if (i > x) {
						dp[x][y][i] = INF;
						continue;
					}
					if (houses[x - 1] != 0) {
						if (houses[x - 1] == y) {
							int temp = INF;
							for(int z = 1; z <= n; z ++) {
								if (z != y) {
									temp = Math.min(temp, dp[x - 1][z][i-1]);
								}
							}
							dp[x][y][i] = Math.min(temp, dp[x - 1][y][i]);
						} else {
							dp[x][y][i] = INF;
						}
					} else {
						int temp = INF;
						for(int z = 1; z <= n; z ++) {
							if (z != y) {
								temp = Math.min(temp, dp[x - 1][z][i-1]);
							}
						}
						dp[x][y][i] = Math.min(temp, dp[x - 1][y][i]) + cost[x - 1][y - 1];
					}
				}
			}
		}
		for (int i = 1; i <= n; i++) {
			result = Math.min(result, dp[m][i][target]);
		}
		return result == INF ? -1 : result;
	}

	public static void main(String[] args) {
//		System.out.println(minCost(new int[]{3,1,2,3}, new int[][]{{1,1,1},{1,1,1},{1,1,1},{1,1,1}}, 4, 3, 3));
//		System.out.println(minCost(new int[]{0,0,0,0,0}, new int[][]{{1,10},{10,1},{10,1},{1,10},{5,1}}, 5, 2, 3));
//		System.out.println(minCost(new int[]{0,0,0,0,0}, new int[][]{{1,10},{10,1},{10,1},{1,10},{5,1}}, 5, 2, 3));
//		System.out.println(minCost(new int[]{0,2,1,2,0}, new int[][]{{1,10},{10,1},{10,1},{1,10},{5,1}}, 5, 2, 3));
		System.out.println(minCostDp(new int[]{0,0,0,1}, new int[][]{{1,5},{4,1},{1,3},{4,4}}, 4, 2, 4));
	}
}
