package cn.echohawk.problem1738;

import com.alibaba.fastjson.JSON;

import java.util.*;

/**
 * @description: 给你一个二维矩阵 matrix 和一个整数 k ，矩阵大小为 m x n 由非负整数组成。
 * 矩阵中坐标 (a, b) 的 值 可由对所有满足 0 <= i <= a < m 且 0 <= j <= b < n 的元素 matrix[i][j]（
 * 下标从 0 开始计数）执行异或运算得到。  请你找出 matrix 的所有坐标中第 k 大的值（k 的值从 1 开始计数）。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/find-kth-largest-xor-coordinate-value
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-19 16:24
 */
public class Solution {
	public static int kthLargestValue(int[][] matrix, int k) {
		int total = matrix.length * matrix[0].length;
		boolean isMax = (total >= 2 * k) ? true : false;
		k = isMax ? k : total - k + 1;
		Deque<Integer> maxdeque = new ArrayDeque<>(k);
		Deque<Integer> minDeque = new ArrayDeque<>(k);
		int dp[][] = new int[matrix.length + 1][matrix[0].length + 1];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				dp[i + 1][j + 1] = matrix[i][j] ^ dp[i][j + 1] ^ dp[i + 1][j] ^ dp[i][j];
				while (!maxdeque.isEmpty() && dp[i + 1][j + 1] > maxdeque.peekLast()) {
					minDeque.addLast(maxdeque.pollLast());
				}
				System.out.print("minDeque" + JSON.toJSONString(minDeque));
				System.out.print(" maxdeque" + JSON.toJSONString(maxdeque));
				while (!minDeque.isEmpty() && dp[i + 1][j + 1] < minDeque.peekLast()) {
					maxdeque.addLast(minDeque.pollLast());
				}
				maxdeque.addLast(dp[i + 1][j + 1]);
				System.out.print("minDeque" + JSON.toJSONString(minDeque));
				System.out.print(" maxdeque" + JSON.toJSONString(maxdeque));
				System.out.println(" k" + k);
				if (minDeque.size() + maxdeque.size() > k) {
					if (isMax) {
						if (minDeque.isEmpty()) {
							maxdeque.pollLast();
						} else {
							minDeque.pollFirst();
						}
					} else {
						if (maxdeque.isEmpty()) {
							minDeque.pollLast();
						} else {
							maxdeque.pollFirst();
						}
					}
				}
			}
		}
		return isMax ? minDeque.isEmpty() ? maxdeque.peekLast() : minDeque.peekFirst() : maxdeque.isEmpty() ? minDeque.peekLast() : maxdeque.peekFirst();
	}

	public static int kthLargestValueInter(int[][] matrix, int k) {
		List<Integer> result = new LinkedList<>();
		int temp = -1;
		int dp[][] = new int[matrix.length + 1][matrix[0].length + 1];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				dp[i + 1][j + 1] = matrix[i][j] ^ dp[i][j + 1] ^ dp[i + 1][j] ^ dp[i][j];
				if (dp[i + 1][j + 1] < temp) {
					continue;
				}
				if (result.size() > k * 1.5) {
					Collections.sort(result, Collections.reverseOrder());
					result.subList(0, k - 1);
					temp = result.get(k - 1);
				}
				result.add(dp[i + 1][j + 1]);
			}
		}
		Collections.sort(result, Collections.reverseOrder());
		return result.get(k - 1);
	}

	public static void main(String[] args) {
		System.out.println(kthLargestValue(new int[][]{{2,5},{1,6}}, 1));
	}
}
