package cn.echohawk.problem81;

/**
 * @description: 已知存在一个按非降序排列的整数数组 nums ，数组中的值不必互不相同。
 * 在传递给函数之前，nums 在预先未知的某个下标 k（0 <= k < nums.length）上进行了 旋转 ，
 * 使数组变为 [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]]（下标 从 0 开始 计数）。
 * 例如， [0,1,2,4,4,4,5,6,6,7] 在下标 5 处经旋转后可能变为 [4,5,6,6,7,0,1,2,4,4] 。
 * 给你 旋转后 的数组 nums 和一个整数 target ，请你编写一个函数来判断给定的目标值是否存在于数组中。
 * 如果 nums 中存在这个目标值 target ，则返回 true ，否则返回 false 。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/search-in-rotated-sorted-array-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-07 09:36
 */
public class Solution {
	public static boolean search(int[] nums, int target) {
		int left = 0, right = nums.length - 1, mid = left + ((right - left) >> 1);
		if (nums[left] == nums[right]) {
			while (left < right && nums[left] == nums[left+1]) {
				left++;
			}
			left++;
		}
		if (nums[right] == target || nums[0] == target || (left < nums.length && nums[left] == target)) {
			return true;
		}
		while (left + 1 < right) {
			if (target == nums[mid]) {
				return true;
			}
			if (target < nums[nums.length - 1]) {
				if (target > nums[mid] || nums[mid] > nums[nums.length - 1]) {
					left = mid;
				} else {
					right = mid;
				}
			} else {
				if (target < nums[mid] || nums[mid] < nums[0]) {
					right = mid;
				} else {
					left = mid;
				}
			}
			mid = left + ((right - left) >> 1);
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println(search(new int[]{1,1,1,1,1,1,1,1,1,13,1,1,1,1,1,1,1,1,1,1,1,1}, 13));
	}
}
