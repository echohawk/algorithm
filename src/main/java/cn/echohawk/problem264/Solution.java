package cn.echohawk.problem264;

import java.util.*;

/**
 * 给你一个整数 n ，请你找出并返回第 n 个 丑数 。
 *
 * 丑数 就是只包含质因数 2、3 和/或 5 的正整数。
 */
public class Solution {
    public static int nthUglyNumber(int n) {
//        if (n == 1) {
//            return 1;
//        }
//        int[] value = new int[]{0,2,3,4,5,6,8,9,10,12,14,15,16,18,20,21,22,24,25,26,27,28};
//        int cycle = n / 22;
//        int offset = n % 22;
//        int result = cycle * 30;
//        return result + value[offset - 1];
        int[] dp = new int[n];
        dp[0] = 1;
        int p1 = 0,p2 = 0,p3 = 0;
        for (int i = 1; i < n; i++) {
            dp[i] = Math.min(dp[p1] * 2, Math.min(dp[p2] * 3, dp[p3] * 5));
            if (dp[i] == dp[p1] * 2) p1++;
            if (dp[i] == dp[p2] * 3) p2++;
            if (dp[i] == dp[p3] * 5) p3++;
        }
        return dp[n - 1];
    }

    public static void main(String[] args) {
        System.out.println(nthUglyNumber(10));
    }
}
