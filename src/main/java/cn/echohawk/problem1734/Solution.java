package cn.echohawk.problem1734;

/**
 * @description: 给你一个整数数组 perm ，它是前 n 个正整数的排列，且 n 是个 奇数 。
 * 它被加密成另一个长度为 n - 1 的整数数组 encoded ，满足 encoded[i] = perm[i] XOR perm[i + 1] 。
 * 比方说，如果 perm = [1,3,2] ，那么 encoded = [2,1] 。
 * 给你 encoded 数组，请你返回原始数组 perm 。题目保证答案存在且唯一。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/decode-xored-permutation
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-05-11 11:47
 */
public class Solution {
	public static int[] decode(int[] encoded) {
		int[] result = new int[encoded.length + 1];
		int allXOR = sumXOR(encoded.length + 1);
		int allEncodedXOR = 0;
		for (int i = 0; i < encoded.length; i ++) {
			allEncodedXOR ^= encoded[i];
		}
		allXOR ^= allEncodedXOR;
		for (int i = 0; i < encoded.length/2 - 1; i ++) {
			allEncodedXOR = (allEncodedXOR ^ encoded[i] ^ encoded[encoded.length - 1 - i]);
			allXOR ^=allEncodedXOR;
		}
		System.out.println(allXOR);
		result[encoded.length/2] = allXOR;
		for (int i = encoded.length/2 - 1; i > -1; i--) {
			result[i] = result[i + 1] ^ encoded[i];
			result[encoded.length - i] = result[encoded.length - 1 - i] ^ encoded[encoded.length - 1 - i];
		}
		return result;
	}

	public static int[] decodeOther(int[] encoded) {
		int[] result = new int[encoded.length + 1];
		int allXOR = sumXOR(encoded.length + 1);
		int allEncodedXOR = 0;
		for (int i = 0; i < encoded.length; i = i +2) {
			allEncodedXOR ^= encoded[i];
		}
		allXOR ^= allEncodedXOR;
		result[encoded.length] = allXOR;
		for (int i = encoded.length - 1; i > -1; i --) {
			result[i] = result[i + 1] ^ encoded[i];
		}
		return result;
	}

	public static int sumXOR(int n) {
		int n4 = n % 4;
		if (n4 == 0) {
			return n;
		} else if (n4 == 1) {
			return 1;
		} else if (n4 == 2) {
			return n + 1;
		}
		return 0;
	}

	public static void main(String[] args) {
		decode(new int[]{6,5,4,6});
	}
}
