package cn.echohawk.problem5779;

public class Solution {
    public boolean isCovered(int[][] ranges, int left, int right) {
        for (int i = left; i <= right; i ++) {
            for (int i1 = 0; i1 < ranges.length; i1++) {
                if (ranges[i1][0] <= i && ranges[i1][1] >= i) {
                    i = ranges[i1][1] + 1;
                    break;
                } else if (i1 == ranges.length - 1) {
                    return false;
                }
            }
        }
        return true;
    }
}
