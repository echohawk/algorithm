package cn.echohawk.problem503;

import com.alibaba.fastjson.JSON;

import java.sql.SQLOutput;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

/**
 * 给定一个循环数组（最后一个元素的下一个元素是数组的第一个元素），输出每个元素的下一个更大元素。
 * 数字 x 的下一个更大的元素是按数组遍历顺序，这个数字之后的第一个比它更大的数，
 * 这意味着你应该循环地搜索它的下一个更大的数。如果不存在，则输出 -1。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/next-greater-element-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    public static int[] nextGreaterElements(int[] nums) {
        Deque<Integer> stack = new LinkedList<>();
        //初始化数据
        stack.addLast(nums[nums.length - 1]);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] > stack.getLast()) {
                stack.addLast(nums[i]);
            }
        }
        System.out.println(JSON.toJSONString(stack));
        for (int i = nums.length - 1; i >= 0; i--) {
            while (!stack.isEmpty() && stack.getFirst() <= nums[i]) {
                stack.pop();
            }
            int temp = nums[i];
            nums[i] = stack.isEmpty() ? -1 : stack.getFirst();
            stack.push(temp);
        }
        return nums;
    }

    public static void main(String[] args) {
        System.out.println(nextGreaterElements(new int[]{0,26,3,4,5,6,3,4,5}));
    }
}
