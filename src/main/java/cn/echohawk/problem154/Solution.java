package cn.echohawk.problem154;

/**
 * 已知一个长度为 n 的数组，预先按照升序排列，经由 1 到 n 次 旋转 后，得到输入数组。例如，原数组 nums = [0,1,4,4,5,6,7] 在变化后可能得到：
 * 若旋转 4 次，则可以得到 [4,5,6,7,0,1,4]
 * 若旋转 7 次，则可以得到 [0,1,4,4,5,6,7]
 * 注意，数组 [a[0], a[1], a[2], ..., a[n-1]] 旋转一次 的结果为数组 [a[n-1], a[0], a[1], a[2], ..., a[n-2]] 。
 *
 * 给你一个可能存在 重复 元素值的数组 nums ，它原来是一个升序排列的数组，并按上述情形进行了多次旋转。请你找出并返回数组中的 最小元素 。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/find-minimum-in-rotated-sorted-array-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 */
public class Solution {
    public static int findMin(int[] nums) {
        int left = 0, right = nums.length - 1;
        //首先如果头尾相等的话 去除头
        if (nums[0] == nums[right]) {
            while(left < right && nums[left] == nums[right]) {
                left++;
            }
        }
        int mid = left + ((right - left) >> 1), min = Math.min(nums[left], nums[right]);
        //加上判断 避免无用功
        if (nums[left] == min) {
            return min;
        }
        while (left + 1 < right) {
            if (nums[mid] > min) {
                left = mid;
            } else {
                right = mid;
                min = nums[right];
            }
            mid = left + ((right - left) >> 1);
        }
        return min;
    }

    public static void main(String[] args) {
        System.out.println(findMin(new int[]{3,1,3}));
    }
}
