package cn.echohawk.problem5786;

public class Solution {
    public int numberOfRounds(String startTime, String finishTime) {
        String[] start = startTime.split(":");
        String[] finish = finishTime.split(":");
        return getMin(start[0], start[1], finish[0], finish[1]);
    }

    private int getMin(String startHour, String startMin, String endHour, String endMin) {
        int endHourInt = Integer.parseInt(endHour);
        int startHourInt = Integer.parseInt(startHour);
        int startMinInt = Integer.parseInt(startMin);
        int endMinInt = Integer.parseInt(endMin);
        if (startHourInt < endHourInt) {
            return ((60 - startMinInt) / 15) +  (endHourInt - startHourInt - 1) * 4 + endMinInt / 15;
        } else if (startHourInt == endHourInt) {
            if (startMinInt < endMinInt) {
                return endMinInt / 15 - (startMinInt / 15 + 1);
            } else {
                return 96 - ((startMinInt / 15 + 1) + endMinInt / 15);
            }
        } else {
            return 96 - (startMinInt / 15 + 1) - (4 - endMinInt / 15) - (startHourInt - endHourInt - 1) * 4;
        }
    }
}
