package cn.echohawk.problem1011;

import java.util.Arrays;

/**
 * @description: 传送带上的包裹必须在 D 天内从一个港口运送到另一个港口。
 * 传送带上的第 i 个包裹的重量为 weights[i]。每一天，我们都会按给出重量的顺序往传送带上装载包裹。
 * 我们装载的重量不会超过船的最大运载重量。  返回能在 D 天内将传送带上的所有包裹送达的船的最低运载能力。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/capacity-to-ship-packages-within-d-days
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-26 09:44
 */
public class Solution {
	public static int shipWithinDays(int[] weights, int D) {
		int[][] dp = new int[weights.length][D];
		dp[0][0] = weights[0];
		for (int i = 1; i < weights.length; i++) {
			dp[i][0] = dp[i - 1][0] + weights[i];
		}
		for (int m = 1; m < D; m++) {
			for (int n = m; n < weights.length; n++) {
				dp[n][m] = Math.max(dp[n - 1][m - 1], weights[n]);
				for (int s = n - 2; s >= m - 1; s--) {
					if (dp[s][m - 1] >= (dp[n][0] - dp[s][0])) {
						dp[n][m] = Math.min(dp[n][m], dp[s][m - 1]);
					} else {
						dp[n][m] = Math.min(dp[n][m], dp[n][0] - dp[s][0]);
					}
				}
			}
		}
		return dp[weights.length - 1][D - 1];
	}

	public static int shipWithinDaysLowSpace(int[] weights, int D) {
		int[][] dp = new int[3][weights.length];
		dp[0][0] = weights[0];
		for (int i = 1; i < weights.length; i++) {
			dp[0][i] = dp[0][i - 1] + weights[i];
		}
		for (int m = 1; m < D; m++) {
			int dpIndex = m > 2 ? 2 : m;
			for (int n = m; n < weights.length; n++) {
				dp[dpIndex][n] = Math.max(dp[dpIndex - 1][n - 1], weights[n]);
				for (int s = n - 2; s >= m - 1; s--) {
					if (dp[dpIndex - 1][s] >= (dp[0][n] - dp[0][s])) {
						dp[dpIndex][n] = Math.min(dp[dpIndex][n], dp[dpIndex - 1][s]);
					} else {
						dp[dpIndex][n] = Math.min(dp[dpIndex][n], dp[0][n] - dp[0][s]);
					}
				}
			}
			System.arraycopy(dp[dpIndex], m, dp[1], m, weights.length - m);
		}
		return dp[D > 2 ? 2 : D - 1][weights.length - 1];
	}

	public static int bestshipWithinDays(int[] weights, int D) {
		int[] sum = new int[weights.length];
		sum[0] = weights[0];
		int left = weights[0];
		for (int i = 1; i < weights.length; i++) {
			sum[i] = sum[i - 1] + weights[i];
			left = Math.max(left, weights[i]);
		}
		int right = sum[weights.length - 1];
		while(left < right) {
			int mid = left + ((right - left) >> 1);
			int condition = 0;
			int index = -1;
			while (index > sum.length - 1) {
				index = binarySearch(sum, index == -1 ? 0 : sum[index - 1], mid, index == -1 ? 0 : index, sum.length - 1) + 1;
				condition++;
			}
			System.out.println("mid" + mid + " condition" + condition);
			if (condition > D) {
				left = mid + 1;
			} else if (condition <= D) {
				right = mid;
			}
		}
		return right;
	}


	public static int bestshipWithin(int[] weights, int D) {
		int right = weights[0];
		int left = weights[0];
		for (int i = 1; i < weights.length; i++) {
			right += weights[i];
			left = Math.max(left, weights[i]);
		}
		while(left < right) {
			int mid = left + ((right - left) >> 1);
			int condition = 1;
			int tempSum = 0;
			for (int weight : weights) {
				if (tempSum + weight > mid) {
					condition++;
					tempSum = 0;
				}
				tempSum += weight;
			}
			if (condition > D) {
				left = mid + 1;
			} else if (condition <= D) {
				right = mid;
			}
		}
		return right;
	}

	private static int binarySearch(int[] arr, int base, int offset, int start, int end) {
		while (start < end) {
			System.out.println("start" + start + " end" + end);
			int mid = end - ((end - start) >> 1);
			if (arr[mid] - base > offset) {
				end = mid - 1;
			} else if (arr[mid] - base < offset) {
				start = mid;
			} else {
				return mid;
			}
		}
		return start;
	}


	public static void main(String[] args) {
		System.out.println(bestshipWithin(new int[]{214,376,385,64,203,394,117,305,297,253,110,470,340,388,482,182,341,43,184,314,265,418,332,111,18,263,4,243
		}, 14));
		System.out.println(shipWithinDaysLowSpace(new int[]{214,376,385,64,203,394,117,305,297,253,110,470,340,388,482,182,341,43,184,314,265,418,332,111,18,263,4,243
		}, 14));
//		System.out.println(binarySearch(new int[]{1,2,3,4,5,6,7,8,9,10}, 0, 4, 4, 9));
	}
}
