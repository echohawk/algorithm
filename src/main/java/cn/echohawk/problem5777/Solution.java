package cn.echohawk.problem5777;

import com.alibaba.fastjson.JSON;

import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    public static int reductionOperations(int[] nums) {
//        Deque<Integer> maxMeque = new LinkedList<>();
//        Deque<Integer> minMeque = new LinkedList<>();
//        int result = 0;
//        for (int num : nums) {
//            while (!minMeque.isEmpty() && minMeque.peek() > num) {
//                maxMeque.push(minMeque.pop());
//            }
//            while (!maxMeque.isEmpty() && maxMeque.peek() <= num) {
//                minMeque.push(maxMeque.pop());
//            }
//            System.out.println(JSON.toJSONString(maxMeque));
//            System.out.println(JSON.toJSONString(minMeque));
//            result += maxMeque.size() + minMeque.size();
//            maxMeque.push(num);
//        }
        int result = 0;
        List<Integer> list = Arrays.stream(nums).boxed().collect(Collectors.toList());
        Collections.sort(list);
        for (int i = list.size() - 1; i > 0; i--) {
            if (list.get(i) > list.get(i - 1)) {
                result += (list.size() - i);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(reductionOperations(new int[]{5,1,3}));
    }
}
