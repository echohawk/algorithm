package cn.echohawk.problem1482;

import java.util.Arrays;

/**
 * 给你一个整数数组 bloomDay，以及两个整数 m 和 k 。
 *
 * 现需要制作 m 束花。制作花束时，需要使用花园中 相邻的 k 朵花 。
 *
 * 花园中有 n 朵花，第 i 朵花会在 bloomDay[i] 时盛开，恰好 可以用于 一束 花中。
 *
 * 请你返回从花园中摘 m 束花需要等待的最少的天数。如果不能摘到 m 束花则返回 -1 。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/minimum-number-of-days-to-make-m-bouquets
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Solution {
    /**
     * 执行用时：
     * 47 ms
     * , 在所有 Java 提交中击败了
     * 7.94%
     * 的用户
     * 内存消耗：
     * 48.2 MB
     * , 在所有 Java 提交中击败了
     * 7.94%
     * 的用户   我不能接受。。。
     * @param bloomDay
     * @param m
     * @param k
     * @return
     */
    //计算出一个以某束花为起点的一个数组 二分
    public static int minDays(int[] bloomDay, int m, int k) {
        int result = -1;
        int[] maxBloom = new int[bloomDay.length - k + 1];
        int max = -1;
        for (int i = 0; i < maxBloom.length; i++) {
            if (i - 1 < 0 || bloomDay[i - 1] == max) {
                max = -1;
                for (int j = 0;j < k; j++) {
                    max = Math.max(max, bloomDay[i + j]);
                }
            } else if (bloomDay[i + (k - 1)] > max){
                max = bloomDay[i + (k - 1)];
            }
            maxBloom[i] = max;
        }
        Arrays.sort(bloomDay);
        int left = 0;
        int right = bloomDay.length;
        while (left < right) {
            int mid = left + ((right - left) >> 1);
            System.out.println(mid);
            int mark = 0;
            boolean can = false;
            for (int i = 0; i < maxBloom.length; i++) {
                if (maxBloom[i] <= bloomDay[mid]) {
                    mark++;
                    i+=(k-1);
                }
                if (m == mark) {
                    can = true;
                    result = bloomDay[mid];
                    break;
                }
            }
            if (can) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return result;
    }

    //计算出一个以某束花为起点的一个数组 二分
    public static int minDaysNoSort(int[] bloomDay, int m, int k) {
        int result = -1;
        int[] maxBloom = new int[bloomDay.length - k + 1];
        int max = -1;
        int maxMax = -1;
        for (int i = 0; i < maxBloom.length; i++) {
            if (i - 1 < 0 || bloomDay[i - 1] == max) {
                max = -1;
                for (int j = 0;j < k; j++) {
                    max = Math.max(max, bloomDay[i + j]);
                }
            } else if (bloomDay[i + (k - 1)] > max){
                max = bloomDay[i + (k - 1)];
            }
            maxMax = Math.max(maxMax, max);
            maxBloom[i] = max;
        }
        int left = 1;
        int right = maxMax + 1;
        while (left < right) {
            int mid = left + ((right - left) >> 1);
            System.out.println(mid);
            int mark = 0;
            boolean can = false;
            for (int i = 0; i < maxBloom.length; i++) {
                if (maxBloom[i] <= mid) {
                    mark++;
                    i+=(k-1);
                }
                if (m == mark) {
                    can = true;
                    result = mid;
                    break;
                }
            }
            if (can) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(minDays(new int[]{1,10,3,10,2
        }, 3, 2));
    }
}
