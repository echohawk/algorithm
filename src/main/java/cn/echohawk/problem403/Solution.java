package cn.echohawk.problem403;

import java.util.*;

/**
 * @description: 一只青蛙想要过河。 假定河流被等分为若干个单元格，并且在每一个单元格内都有可能放有一块石子（也有可能没有）。
 * 青蛙可以跳上石子，但是不可以跳入水中。  给你石子的位置列表 stones（用单元格序号 升序 表示）， 
 * 请判定青蛙能否成功过河（即能否在最后一步跳至最后一块石子上）。
 * 开始时， 青蛙默认已站在第一块石子上，并可以假定它第一步只能跳跃一个单位（即只能从单元格 1 跳至单元格 2 ）。
 * 如果青蛙上一步跳跃了 k 个单位，那么它接下来的跳跃距离只能选择为 k - 1、k 或 k + 1 个单位。 
 * 另请注意，青蛙只能向前方（终点的方向）跳跃。   
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/frog-jump
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-29 12:11
 */
public class Solution {
	public static boolean canCross(int[] stones) {
		if (stones.length < 3) {
			return true;
		}
		Map<Integer, List<Integer>> dpMap = new HashMap<>();
		addMap(dpMap, stones[1], stones[1] - stones[0]);
		for (int i = 2; i < stones.length - 1; i++) {
			List<Integer> integers = dpMap.get(stones[i]);
			if (integers != null) {
				for (int i1 = 0; i1 < integers.size(); i1++) {
					addMap(dpMap, stones[i], integers.get(i1));
				}
				dpMap.remove(stones[i]);
			}
		}
		return dpMap.containsKey(stones[stones.length - 1]);
	}

	private static void addMap(Map<Integer, List<Integer>> dpMap, int fundation, int gap) {
		List<Integer> objects = dpMap.get(fundation + gap);
		if (objects == null) {
			objects = new ArrayList<Integer>();
		}
		objects.add(gap);
		dpMap.put(fundation + gap, objects);
		if (gap != 1) {
			List<Integer> objects1 = dpMap.get(fundation + gap - 1);
			if (objects1 == null) {
				objects1 = new ArrayList<Integer>();
			}
			objects1.add(gap - 1);
			dpMap.put(fundation + gap - 1, objects1);
		}
		List<Integer> objects2 = dpMap.get(fundation + gap + 1);
		if (objects2 == null) {
			objects2 = new ArrayList<Integer>();
		}
		objects2.add(gap + 1);
		dpMap.put(fundation + gap + 1, objects2);
	}

	public static boolean canCrossMyTry(int[] stones) {
		if (stones[1] != 1) {
			return false;
		}
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(1);
		a : for (int i = 2; i < stones.length; i++) {
			int jump = stones[i] - stones[i - 1];
			while (stack.size() > 3 && stack.peek() + 1 < jump) {
				Integer pop = stack.pop();
				stack.push(stack.pop() + pop);
			}
			if (stack.peek() >= jump - 1 && stack.peek() <= jump + 1) {
				stack.push(jump);
			} else {
				while (stack.size() > 1) {
					jump += stack.pop();
					if (stack.peek() >= jump - 1 && stack.peek() <= jump + 1) {
						stack.push(jump);
						continue a;
					}
				}
				return false;
			}
		}
		return true;
	}

	public static boolean canCrossMyTry1(int[] stones) {
		int length = stones.length;
		boolean dp[][] = new boolean[length][length];
		dp[0][0] = true;
		for (int i = 1; i < stones.length; i++) {
			for (int i1 = 0; i1 < i; i1++) {
				int jump = stones[i] - stones[i1];
				if (jump > i1 + 1) {
					continue;
				}
				dp[i][jump] = dp[i1][jump - 1] || dp[i1][jump] || dp[i1][jump + 1];
				if (i == length - 1 && dp[i][jump]) {
					return true;
				}
			}
		}
		return false;
	}


	public static void main(String[] args) {
//		System.out.println(canCrossMyTry(new int[]{0,1,2,3,4,5,6,12}));
//		System.out.println(canCrossMyTry(new int[]{0,1,3,6,10,13,14}));
//		System.out.println(canCrossMyTry(new int[]{0,1,3,6,10,15,16,21}));
		System.out.println(canCrossMyTry1(new int[]{0,1,3,5,6,8,12,17}));
	}
}
