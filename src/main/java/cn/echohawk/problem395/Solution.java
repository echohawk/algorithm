package cn.echohawk.problem395;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 给你一个字符串 s 和一个整数 k ，请你找出 s 中的最长子串， 要求该子串中的每一字符出现次数都不少于 k 。返回这一子串的长度。
 * @author: echohawk
 * @create: 2021-06-07 17:16
 */
public class Solution {
	public int longestSubstring(String s, int k) {
		int result = 0;
		char[] chars = s.toCharArray();
		Map<Character, Integer> map = new HashMap<>();
		int mark = 0;
		for (int i = 0; i < chars.length; i++) {
			for (int j = i; j < chars.length; j++) {
				Integer orDefault = map.getOrDefault(chars[j], 0);
				if (orDefault.equals(0)) mark++;
				orDefault += 1;
				if (orDefault.equals(k)) mark--;
				map.put(chars[j], orDefault);
				if (mark == 0) {
					result = Math.max(result, j - i + 1);
				}
			}
			map.clear();
			mark = 0;
		}
		return result;
	}
}
