package cn.echohawk.problem5783;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {
    public static int maximumRemovals(String s, String p, int[] removable) {
        int result = -1;
        char[] chars = p.toCharArray();
        char[] sChar = s.toCharArray();
        List<Integer> indexTemp = new ArrayList<>(p.length());
        for (int i = 0; i < removable.length; i++) {
            char c = sChar[removable[i]];
            sChar[removable[i]] = ' ';
            if (indexTemp.size() == 0 || indexTemp.contains(removable[i])) {
                int index = 0;
                for (char aChar : chars) {
                    if (index >= sChar.length) return result + 1;
                    for (int i1 = index; i1 < sChar.length; i1++) {
                        if (sChar[i1] == aChar) {
                            if (indexTemp.size() == p.length()) {
                                indexTemp.clear();
                            }
                            indexTemp.add(i1);
                            index = i1 + 1;
                            break;
                        } else if (i1 == sChar.length - 1) {
                            return result + 1;
                        }
                    }
                }
            }
            result = i;
        }
        return result + 1;
    }

    public static void main(String[] args) {
//        "rqmvwezfxczzeqokjww"
//        "rezxczzeqw"
//                [18,1,3,7,4,16,14,2,15,0,6,12,17,11,13,5,9]
        System.out.println(maximumRemovals("rqmvwezfxczzeqokjww", "rezxczzeqw", new int[]{18,1,3,7,4,16,14,2,15,0,6,12,17,11,13,5,9}));
    }
}
