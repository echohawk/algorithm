package cn.echohawk.problem5780;

public class Solution {
    public int chalkReplacer(int[] chalk, int k) {
        int[] sum = new int[chalk.length];
        sum[0] = chalk[0];
        for (int i = 1; i < chalk.length; i++) {
            if (k - sum[i - 1] < chalk[i]) {
                break;
            }
            sum[i] = sum[i - 1] + chalk[i];
        }
        int mod = sum[chalk.length - 1] != 0 ? k % sum[chalk.length - 1] : k;
        for (int i = 0; i < chalk.length; i++) {
            if (mod < sum[i]) {
                return i;
            }
        }
        return -1;
    }
}
