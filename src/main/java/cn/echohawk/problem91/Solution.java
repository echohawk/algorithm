package cn.echohawk.problem91;

/**
 * @description: 一条包含字母 A-Z 的消息通过以下映射进行了
 * 编码 ：  'A' -> 1 'B' -> 2 ... 'Z' -> 26
 * 要 解码 已编码的消息，所有数字必须基于上述映射的方法，反向映射回字母（可能有多种方法）。
 * 例如，"11106" 可以映射为：  "AAJF" ，将消息分组为 (1 1 10 6) "KJF" ，
 * 将消息分组为 (11 10 6) 注意，消息不能分组为  (1 11 06) ，
 * 因为 "06" 不能映射为 "F" ，这是由于 "6" 和 "06" 在映射中并不等价。
 * 给你一个只含数字的 非空 字符串 s ，请计算并返回 解码 方法的 总数 。
 * 题目数据保证答案肯定是一个 32 位 的整数。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/decode-ways
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @author: echohawk
 * @create: 2021-04-21 12:22
 */
public class Solution {
	private static char[] chars;

	/**
	 * 暴力递归
	 * @param s
	 * @return
	 */
	public static int numDecodings(String s) {
		chars = s.toCharArray();
		if ("".equals(s)) {
			return 0;
		}
		return partNumDecoding(-1);
	}

	private static int partNumDecoding(int from) {
		if (from == chars.length - 1) {
			return 1;
		} else if (from > chars.length - 1) {
			return 0;
		}
		//跨一位
		from ++;
		if (chars[from] == '0') {
			return 0;
		}
		int part1 = partNumDecoding(from);
		//跨两位
		from++;
		int part2 = 0;
		if (from < chars.length && chars[from - 1] < '3' && (chars[from - 1] != '2' || chars[from] < '7')) {
			part2 = partNumDecoding(from);
		}
		return part1 + part2;
	}


	/**
	 * 优雅DP
	 * @param s
	 * @return
	 */
	public static int numDecodingsDp(String s) {
		chars = s.toCharArray();
		int[] dp = new int[chars.length + 1];
		if (chars[0] == '0') {
			return 0;
		}
		dp[0] = 1;
		dp[1] = 1;
		for (int i = 1; i < chars.length; i ++) {
			if (chars[i] != '0') {
				dp[i + 1] += dp[i];
			}
			if (chars[i - 1] == '1' || (chars[i - 1] == '2' && chars[i] < '7')) {
				dp[i + 1] += dp[i - 1];
			}
		}
		return dp[chars.length];
	}


	public static void main(String[] args) {
		System.out.println(numDecodingsDp("111111111111111111111111111111111111111111111")); //1836311903
		System.out.println(numDecodingsDp("2101")); //1
	}
}
