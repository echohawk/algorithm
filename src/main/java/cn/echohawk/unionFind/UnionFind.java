package cn.echohawk.unionFind;

import com.alibaba.fastjson.JSON;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author echohawk
 * @Date 2021-11-03 15:27
 * @description 并查集
 **/
public class UnionFind<T> {
    private List<T> list;
    private Map<T, T> parentMap=new HashMap<>();
    private Map<T, Integer> parentWeightMap=new HashMap<>();
    public UnionFind(List<T> list) {
        this.list = list;
        for (T t : list) {
            parentMap.put(t,t);
            parentWeightMap.put(t,1);
        }
    }

    public T find(T child) {
        if (child == parentMap.get(child)) {
            return child;
        } else {
            T leader = find(parentMap.get(child));
            parentMap.put(child, leader);
            return leader;
        }
    }

    public void union(T union1, T union2) {
        T union1Leader = find(union1);
        T union2Leader = find(union2);
        if (union1Leader == union2Leader) {
            return;
        } else {
            if (parentWeightMap.get(union1Leader) > parentWeightMap.get(union2Leader)) {
                parentMap.put(union2Leader, union1Leader);
                parentWeightMap.put(union1Leader, Math.max(parentWeightMap.get(union1Leader), parentWeightMap.get(union2Leader) + 1));
            } else {
                parentMap.put(union1Leader, union2Leader);
                parentWeightMap.put(union2Leader, Math.max(parentWeightMap.get(union1Leader), parentWeightMap.get(union2Leader) + 1));
            }
        }
    }

    @Override
    public String toString() {
        Map<T, List<T>> printMap = new HashMap<>();
        list.stream().forEach(ele -> {
            List<T> ts = Optional.ofNullable(printMap.get(find(ele))).orElseGet(ArrayList::new);
            ts.add(ele);
            printMap.put(find(ele), ts);
        });
        return JSON.toJSONString(printMap.entrySet().stream().map(mapper -> mapper.getValue()).collect(Collectors.toList()));
    }

    public static void main(String[] args) {
        UnionFind<String> union = new UnionFind<String>(Arrays.asList(new String[]{"1","2","3","4","5","6","7","8","9","10","11","12"}));
        union.union("2", "3");
        union.union("7", "3");
        union.union("7", "11");
        union.union("7", "3");
        union.union("9", "11");
        System.out.println(JSON.toJSONString(union.parentMap));
        System.out.println(JSON.toJSONString(union.parentWeightMap));
        System.out.println(union);
    }
}

