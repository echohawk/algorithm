package cn.echohawk.problem692;

import com.alibaba.fastjson.JSON;

import java.util.*;

/**
 * @description: 给一非空的单词列表，返回前 k 个出现次数最多的单词。
 * 返回的答案应该按单词出现频率由高到低排序。如果不同的单词有相同出现频率，按字母顺序排序。
 * @author: echohawk
 * @create: 2021-05-20 11:00
 */
public class Solution {
	public static List<String> topKFrequent(String[] words, int k) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (String word : words) {
			Integer frequency = map.getOrDefault(word, 0);
			map.put(word, frequency + 1);
		}
		PriorityQueue<Object[]> priorityQueue = new PriorityQueue<Object[]>(k, (Object[] a, Object[] b)-> {
			if(((Integer)a[1]).equals(b[1])) {
				return ((String)b[0]).compareTo((String)a[0]);
			}
			return (Integer) a[1] - (Integer) b[1];
		});
		for (Map.Entry<String, Integer> stringIntegerEntry : map.entrySet()) {
			if (priorityQueue.size() < k) {
				priorityQueue.add(new Object[]{stringIntegerEntry.getKey(), stringIntegerEntry.getValue()});
			} else {
				int flu = (int) priorityQueue.peek()[1];
				String fluString = (String) priorityQueue.peek()[0];
				if (flu < stringIntegerEntry.getValue() || (flu == stringIntegerEntry.getValue() && fluString.compareTo(stringIntegerEntry.getKey()) > 0)) {
					priorityQueue.poll();
					priorityQueue.add(new Object[]{stringIntegerEntry.getKey(), stringIntegerEntry.getValue()});
				}
			}
		}
		List<String> result = new ArrayList<>(k);
		while (!priorityQueue.isEmpty()) {
			result.add((String)priorityQueue.poll()[0]);
		}
		Collections.reverse(result);
		return result;
	}

	public static void main(String[] args) {
		System.out.println(topKFrequent(new String[]{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}, 4));
	}
}
