package cn.echohawk.problem690;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

class Employee {
    public int id;
    public int importance;
    public List<Integer> subordinates;
}
public class Solution {

    public int getImportance(List<Employee> employees, int id) {
        int result = 0;
        Map<Integer, Employee> map = new HashMap<>(employees.size() * 4 / 3 + 1);
        for (Employee employee : employees) {
            map.put(employee.id, employee);
        }
        Stack<Integer> stack = new Stack<>();
        if (map.get(id) != null) {
            stack.push(id);
        }
        while (!stack.isEmpty()) {
            Employee employee = map.get(stack.pop());
            result += employee.importance;
            for (Integer subordinate : employee.subordinates) {
                stack.push(subordinate);
            }
        }
        return result;
    }
}
