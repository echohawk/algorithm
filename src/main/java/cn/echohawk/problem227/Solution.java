package cn.echohawk.problem227;

import com.alibaba.fastjson.JSON;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @description:
 * @author: echohawk
 * @create: 2021-06-02 15:06
 */
public class Solution {
	public int calculate(String s) {
		int left = 0;
		Deque<Integer> dataDeque = new LinkedList<>();
		Deque<Character> strDeque = new LinkedList<>();
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '*' || s.charAt(i) == '/' || s.charAt(i) == '+' || s.charAt(i) == '-' || i == s.length() - 1) {
				dataDeque.push(Integer.parseInt(s.substring(left, i == s.length() - 1 ? s.length() : i).trim()));
				if (!strDeque.isEmpty() && (strDeque.peek() == '*' || strDeque.peek() == '/')) {
					if (strDeque.pop() == '*') {
						dataDeque.push(dataDeque.pop() * dataDeque.pop());
					} else {
						Integer pop = dataDeque.pop();
						dataDeque.push(dataDeque.pop() / pop);
					}
				}
				if (i != s.length() - 1) {
					strDeque.push(s.charAt(i));
					left=i+1;
				}
			}
			System.out.println(JSON.toJSONString(dataDeque));
			System.out.println(JSON.toJSONString(strDeque));
		}
		while (!strDeque.isEmpty()) {
			Character pop = strDeque.pollLast();
			if (pop == '+') {
				dataDeque.addLast(dataDeque.pollLast() + dataDeque.pollLast());
			} else {
				Integer pop1 = dataDeque.pollLast();
				dataDeque.addLast(pop1 - dataDeque.pollLast());
			}
			System.out.println(JSON.toJSONString(dataDeque));
		}
		return dataDeque.peek();
	}
}
